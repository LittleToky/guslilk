import React, {Component} from 'react';

// Material-UI
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import {List, ListItem} from 'material-ui/List';
import {Tabs, Tab} from 'material-ui/Tabs';
//import SelectField from 'material-ui/SelectField';
//import {Toolbar} from 'material-ui/Toolbar';
//import MenuItem from 'material-ui/MenuItem';
//import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import RefreshIndicator from 'material-ui/RefreshIndicator';

// Font
import 'typeface-roboto'

const styles = {
  hidden: {
    display: 'none'
  }
}

const style = {
  container: {
    position: 'relative',
  },
  refresh: {
    display: 'inline-block',
    position: 'relative',
  },
};

class Filter extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      genres: props.genres,
      artists: props.artists,
      songs: props.songs,
      apid: props.apid,
      places: props.places,
      audio: '',
      paused: false,
      searchResults: false
    }
  }

  componentDidMount() {
    this.props.getGenres();
    this.props.getArtists();
    this.props.getSongs();
  }

  componentWillReceiveProps(props) {
    this.setState({'genres': props.genres, 'artists': props.artists, 'songs': props.songs, 'places': props.places, 'apid': props.apid});
  }

  throttle = (v,type) => {
    if (this.throttleVar) {
      clearTimeout(this.throttleVar);
    }
    this.throttleVar=setTimeout(this.throttleFunc,600,v,type)
  }


  throttleFunc = (v,type) => {
    let apid = this.state.apid;
    let t = this;
    let XHR = ("onload" in new XMLHttpRequest())
      ? XMLHttpRequest
      : XDomainRequest;
    let xhr = new XHR();
    xhr.withCredentials = true;
    xhr.open('GET', 'https://betacab.gusli.net/api/v1/search/'+type+'/'+apid+'?needFilter=on&q='+v, true);
    xhr.onload = function() {
      if (JSON.parse(this.responseText).status_code===401) {
        t.logout();
      } else {
        let data=JSON.parse(this.response).data;
        if (data) {
          data.forEach(function(item,i){data[i].visible=true});
          let st=t.state[type];
          let arr=st[apid];
          data.forEach(function(item){
            let l = arr.filter(function(it){
              return it.id===item.id
            }).length;
            if (l===0) {
              item.duration=t.props.durationFormater(item.duration);
              item.visible=true;
              item.title=item.title?item.title:item.name;
              st[apid].push(item);
            }
          });
          let obj={};
          obj[type]=st;
          obj.searchResults=true;
          t.setState(obj);
        }
      }
    }
    xhr.send();
  }

  inputChange = (v, type) => {
    let arr = this.state[type];
    let apid = this.state.apid;
    if (v.length>2 && type!=="genres") {
      this.throttle(v,type);
    }
    arr[apid].forEach(function(item, i) {
      arr[apid][i].visible = item.title.toLowerCase().indexOf(v.toLowerCase()) > -1
    });
    let res = {}
    res[type] = arr;
    this.setState(res);
  }

  play = (url) => {
    let audio=document.getElementById('filterAudio');
    if (url!==audio.src) {
      audio.pause();
      this.setState({'audio':url,'paused':true},function(){
        audio.src=this.state.audio;
        audio.play();
        this.setState({'audio':url,'paused':false});
      });
    } else {
      this.setState({'paused':false});
      audio.play();
    }
  }

  pause = () => {
    let audio=document.getElementById('filterAudio');
    this.setState({'paused':true});
    audio.pause();
  }

  audioEnded = () => {
    this.setState({paused: true});
  }

  tabChange = (v) => {
    this.setState({'searchResults':false});
    if (v!=="getGenres") {
      this.props[v]();
    }
  }

  render() {
    let apid = this.state.apid;
    let genres = this.state.genres;
    let artists = this.state.artists;
    let songs = this.state.songs;
    let audio=document.getElementById('filterAudio');
    let src=this.state.audio;
    let paused=audio?audio.paused:true;
    let searchResults=this.state.searchResults;
    return (
      !!(apid && genres[apid] && artists[apid] && songs[apid])?<div>
        {/*<Toolbar className="toolbar">
          <SelectField value={this.state.apid} onChange={(event, index, id)=>this.props.changeApid(id)}>
            {this.state.places.map((row, index) => (<MenuItem key={index} value={row.id} primaryText={row.title}/>))}
          </SelectField>
          <FlatButton label="Применить ко всем" className="toolbarButton"/>
        </Toolbar>*/}
        <p className="pageHeader">Фильтр музыки</p>
        <p className="smallText">Настройки разрешенной или запрещённой музыки в вашем заведении</p>
        <p className="smallText">Чтобы добавить Песню, Артиста или Жанр в свой отфильтрованный список, просто введите название в поисковой строке и включите соответствующий маркер запрета или разрешения.</p>
        <Tabs className="filterTabs" onChange={(v)=>this.tabChange(v)}>
          <Tab label="По жанрам" value="getGenres">
            <TextField className="genreInput" hintText="Введите название жанра" onChange={(e,v) => this.inputChange(v, 'genres')}/>
            <List className="list">
              {genres[apid]
                ? genres[apid].map((row, index) => (
                  <ListItem key={index} className="genres" style={row.visible
                    ? styles.visible
                    : styles.hidden}>
                    <div className="col">
                      <IconButton>
                        <FontIcon className="fa fa-list-ul"/>
                      </IconButton>
                    </div>
                    <div className="col">
                      {row.title}
                    </div>
                    <div className="col right">
                      <IconButton >
                        <FontIcon className={row.on
                          ? "fa fa-volume-up green"
                          : "fa fa-volume-off red"}
                          onClick={() => {
                            this.props.toggle(row, 'genres', searchResults);
                            //this.setState({'searchResults':false});
                          }}/>
                      </IconButton>
                    </div>
                  </ListItem>
                ))
                : ''}
            </List>
          </Tab>
          <Tab label="По артистам" value="getArtists">
            <TextField className="genreInput" hintText="Введите название артиста" onChange={(e,v) => this.inputChange(v, 'artists')}/>
            <List className="list">
              {artists[apid]
                ? artists[apid].map((row, index) => (
                  <ListItem key={index} style={row.visible
                    ? styles.visible
                    : styles.hidden} className="artists">
                    <div className="col">
                      <IconButton>
                        <FontIcon className="fa fa-user"/>
                      </IconButton>
                    </div>
                    <div className="col">
                      {row.title}
                    </div>
                    <div className="col right">
                      <IconButton >
                        <FontIcon className={row.on
                          ? "fa fa-volume-up green"
                          : "fa fa-volume-off red"}
                          onClick={() => {
                            this.props.toggle(row, 'artists', searchResults);
                            //this.setState({'searchResults':false});
                          }}/>
                      </IconButton>
                    </div>
                  </ListItem>
                ))
                : ''}
            </List>
          </Tab>
          <Tab label="По песням" value="getSongs">
            <TextField className="genreInput" hintText="Введите название песни" onChange={(e,v) => this.inputChange(v, 'songs')}/>
            <List className="list songs">
              {songs[apid]
                ? songs[apid].map((row, index) => (
                  <ListItem key={index} style={row.visible
                    ? styles.visible
                    : styles.hidden} className="songs sb">
                    <div className="col">
                      <IconButton>
                        <FontIcon className="fa fa-music"/>
                      </IconButton>
                    </div>
                    <div className="col">
                      <IconButton className="left">
                          {(paused && (audio && audio.src!=='')) || row.url!==src? //
                            <FontIcon className="fa fa-play-circle" onClick={() => this.play(row.url)}/>:
                            <FontIcon className="fa fa-pause-circle" onClick={() => this.pause()}/>
                          }
                      </IconButton>
                      {row.title}<br/>
                      <div className="subText">{row.artist}</div>
                    </div>
                    <div className="col right">
                      <IconButton >
                        <FontIcon className={row.on
                          ? "fa fa-volume-up green"
                          : "fa fa-volume-off red"} onClick={() => {
                            this.props.toggle(row, 'songs', searchResults);
                            //this.setState({'searchResults':false});
                          }}/>
                      </IconButton>
                    </div>
                    <div className="col right">
                      {row.duration}
                    </div>
                  </ListItem>
                ))
                : ''}
            </List>
          </Tab>
        </Tabs>
        <audio id="filterAudio" onEnded={() => this.audioEnded()}></audio>
      </div>:<RefreshIndicator
      size={50}
      left={70}
      top={0}
      loadingColor="#FF9800"
      status="loading"
      style={style.refresh}
    />
    );
  }
}

export default Filter;
