import React, {Component} from 'react';

// Material-UI
import {Tabs, Tab} from 'material-ui/Tabs';
import {List, ListItem} from 'material-ui/List';
//import MenuItem from 'material-ui/MenuItem';
//import SelectField from 'material-ui/SelectField';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
//import {Toolbar} from 'material-ui/Toolbar';
import RefreshIndicator from 'material-ui/RefreshIndicator';

// Font
import 'typeface-roboto'

const style = {
  container: {
    position: 'relative',
  },
  refresh: {
    display: 'inline-block',
    position: 'relative',
  },
};

class Stats extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      placeStats: this.props.placeStats,
      places: this.props.places,
      apid: props.apid,
      audio: '',
      paused: false
    }
  }

  componentDidMount() {
    this.props.getPlaceStats();
  }

  componentWillReceiveProps(props) {
    this.setState({'placeStats': props.placeStats, 'places': props.places, 'apid': props.apid});
  }

  play = (url) => {
    let audio=document.getElementById('filterAudio');
    if (url!==audio.src) {
      audio.pause();
      this.setState({'audio':url,'paused':true},function(){
        audio.src=this.state.audio;
        audio.play();
        this.setState({'audio':url,'paused':false});
      });
    } else {
      this.setState({'paused':false});
      audio.play();
    }
  }

  pause = () => {
    let audio=document.getElementById('filterAudio');
    this.setState({'paused':true});
    audio.pause();
  }

  audioEnded = () => {
    this.setState({paused: true});
  }

  render() {
    let apid=this.state.apid;
    let songStats = this.state.placeStats[apid];
    let audio=document.getElementById('filterAudio');
    let src=this.state.audio;
    let paused=audio?audio.paused:true;
    return (
      !!(apid && songStats)?<div className="Stats">
        {/*<Toolbar className="toolbar">
          <SelectField value={apid} onChange={(event, index, id) => this.props.changeApid(id)}>
            {this.state.places.map((row, index) => (<MenuItem key={index} value={row.id} primaryText={row.title}/>))}
          </SelectField>
        </Toolbar>*/}
        <div className="col">
          <p className="pageHeader">Статистика</p>
          <Tabs className="statTabs">
            <Tab label="За сегодня" value="a">
              <List className="stats list">
                {songStats
                  ? songStats.today.map((row, index) => (
                    <ListItem key={index}>
                      <div className="col">
                        <IconButton className="left">
                        {(paused && (audio && audio.src!=='')) || row.url!==src? //
                          <FontIcon className="fa fa-play-circle" onClick={() => this.play(row.url)}/>:
                          <FontIcon className="fa fa-pause-circle" onClick={() => this.pause()}/>
                        }
                        </IconButton>
                        {row.title}
                        <div className="subText">{row.artist}</div>
                      </div>
                      <div className="col">
                        <IconButton >
                          <FontIcon className={row.on
                            ? "fa fa-volume-up green"
                            : "fa fa-volume-off red"} onClick={() => this.props.toggle(row, 'today')}/>
                        </IconButton>
                      </div>
                      <div className="col">
                        {row.duration}
                        <div className="subText">{row.date?'в '+row.date.match(/\s(.+):\d\d/)[1]:''}</div>
                      </div>
                    </ListItem>
                  ))
                  : ''}
              </List>
            </Tab>
            <Tab label="За вчера" value="b">
              <List className="stats list">
                {songStats
                  ? songStats.yesterday.map((row, index) => (
                    <ListItem key={index}>
                      <div className="col">
                        <IconButton className="left">
                        {(paused && (audio && audio.src!=='')) || row.url!==src? //
                          <FontIcon className="fa fa-play-circle" onClick={() => this.play(row.url)}/>:
                          <FontIcon className="fa fa-pause-circle" onClick={() => this.pause()}/>
                        }
                        </IconButton>
                        {row.title}
                        <div className="subText">{row.artist}</div>
                      </div>
                      <div className="col">
                        <IconButton >
                          <FontIcon className={row.on
                            ? "fa fa-volume-up green"
                            : "fa fa-volume-off red"} onClick={() => this.props.toggle(row, 'yesterday')}/>
                        </IconButton>
                      </div>
                      <div className="col">
                        {row.duration}
                        <div className="subText">{row.date?'в '+row.date.match(/\s(.+):\d\d/)[1]:''}</div>
                      </div>
                    </ListItem>
                  ))
                  : ''}
              </List>
            </Tab>
            <Tab label="За месяц" value="c">
              <List className="stats list">
                {songStats
                  ? songStats.month.map((row, index) => (
                    <ListItem key={index}>
                      <div className="col">
                        <IconButton className="left">
                        {(paused && (audio && audio.src!=='')) || row.url!==src? //
                          <FontIcon className="fa fa-play-circle" onClick={() => this.play(row.url)}/>:
                          <FontIcon className="fa fa-pause-circle" onClick={() => this.pause()}/>
                        }
                        </IconButton>
                        {row.title}
                        <div className="subText">{row.artist}</div>
                      </div>
                      <div className="col">
                        <IconButton >
                          <FontIcon className={row.on
                            ? "fa fa-volume-up green"
                            : "fa fa-volume-off red"} onClick={() => this.props.toggle(row, 'month')}/>
                        </IconButton>
                      </div>
                      <div className="col">
                        {row.duration}
                        <div className="subText">{row.date?row.date.split(' ')[0].split('-').reverse().join('.')+' в '+row.date.split(' ')[1].slice(0,5):''}</div>
                      </div>
                    </ListItem>
                  ))
                  : ''}
              </List>
            </Tab>
          </Tabs>
        </div>
        <div className="col aside">
          <p className="pageHeader">Заказы</p>
          <List className="activityList">
            <ListItem className='listItem' disabled={true}>
              <div className="col">Сутки</div>
              <div className="col">{songStats
                  ? songStats.orders.day
                  : ''}</div>
            </ListItem>
            <ListItem className='listItem' disabled={true}>
              <div className="col">Неделя</div>
              <div className="col">{songStats
                  ? songStats.orders.week
                  : ''}</div>
            </ListItem>
            <ListItem className='listItem' disabled={true}>
              <div className="col">Месяц</div>
              <div className="col">{songStats
                  ? songStats.orders.month
                  : ''}</div>
            </ListItem>
          </List>
        </div><audio id="filterAudio" onEnded={() => this.audioEnded()}></audio>
      </div>:<RefreshIndicator
      size={50}
      left={70}
      top={0}
      loadingColor="#FF9800"
      status="loading"
      style={style.refresh}
    />
    );
  }
}

export default Stats;
