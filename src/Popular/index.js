import React, {Component} from 'react';

// Material-UI
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import {List, ListItem} from 'material-ui/List';
//import SelectField from 'material-ui/SelectField';
//import {Toolbar} from 'material-ui/Toolbar';
//import MenuItem from 'material-ui/MenuItem';
import RefreshIndicator from 'material-ui/RefreshIndicator';

// Font
import 'typeface-roboto'

const style = {
  container: {
    position: 'relative',
  },
  refresh: {
    display: 'inline-block',
    position: 'relative',
  },
};

class Popular extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      tracks: props.popular,
      places: props.places,
      apid: props.apid,
      audio: '',
      paused: false
    }
  }

  componentDidMount() {
    this.props.getPopular();
  }

  componentWillReceiveProps(props) {
    this.setState({'tracks': props.popular, 'places': props.places, 'apid': props.apid});
  }

  play = (url) => {
    let audio=document.getElementById('filterAudio');
    if (url!==audio.src) {
      audio.pause();
      this.setState({'audio':url,'paused':true},function(){
        audio.src=this.state.audio;
        audio.play();
        this.setState({'audio':url,'paused':false});
      });
    } else {
      this.setState({'paused':false});
      audio.play();
    }
  }

  pause = () => {
    let audio=document.getElementById('filterAudio');
    this.setState({'paused':true});
    audio.pause();
  }

  audioEnded = () => {
    this.setState({paused: true});
  }

  render() {
    let apid=this.state.apid;
    let tracks = this.state.tracks[apid];
    let audio=document.getElementById('filterAudio');
    let src=this.state.audio;
    let paused=audio?audio.paused:true;
    return (
      !!(apid && tracks)?<div>
        {/*}<Toolbar className="toolbar">
          <SelectField value={apid} onChange={(event, index, id) => this.props.changeApid(id)}>
            {this.state.places.map((row, index) => (<MenuItem key={index} value={row.id} primaryText={row.title}/>))}
          </SelectField>
        </Toolbar>*/}
        <p className="pageHeader">Топ популярных</p>
        <p className="smallText">Эти треки популярны в большинстве заведений, рекомендуем разблокировать их.</p>
        <List>
          {tracks
            ? tracks.map((row, index) => (
              <ListItem className="pops sb" key={index}>
                <div className="col">
                  {index + 1 < 10
                    ? ("0" + ((index + 1) + ''))
                    : index + 1}
                </div>
                <div className="col">
                  <IconButton>
                  {(paused && (audio && audio.src!=='')) || row.url!==src? //
                    <FontIcon className="fa fa-play-circle" onClick={() => this.play(row.url)}/>:
                    <FontIcon className="fa fa-pause-circle" onClick={() => this.pause()}/>
                  }
                  </IconButton>
                </div>
                <div className="col">
                  {row.title}
                  <div className="subText">{row.artist}</div>
                </div>
                <div className="col right">
                  <IconButton >
                    <FontIcon className={"fa fa-volume-off red"} onClick={() => this.props.toggle(row)}/>
                  </IconButton>
                </div>
                <div className="col right">
                  {row.duration}
                </div>
              </ListItem>
            ))
            : ''}
        </List><audio id="filterAudio" onEnded={() => this.audioEnded()}></audio>
      </div>:<RefreshIndicator
      size={50}
      left={70}
      top={0}
      loadingColor="#FF9800"
      status="loading"
      style={style.refresh}
    />
    );
  }
}

export default Popular;
