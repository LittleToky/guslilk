import React, {Component} from 'react';
//import Week from '../Week';
import Times from '../Times';

// Material-UI

import RaisedButton from 'material-ui/RaisedButton';
import {Tabs, Tab} from 'material-ui/Tabs';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Toggle from 'material-ui/Toggle';
//import {Toolbar} from 'material-ui/Toolbar';
//import FontIcon from 'material-ui/FontIcon';
//import {List, ListItem} from 'material-ui/List';
//import Checkbox from 'material-ui/Checkbox';
//import IconButton from 'material-ui/IconButton';
// import {
//   Table,
//   TableBody,
//   TableHeader,
//   TableHeaderColumn,
//   TableRow,
//   TableRowColumn
// } from 'material-ui/Table';
import RefreshIndicator from 'material-ui/RefreshIndicator';

// Font
import 'typeface-roboto'

const style = {
  container: {
    position: 'relative',
  },
  refresh: {
    display: 'inline-block',
    position: 'relative',
  },
};

class PlaceSettings extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      pwdVis: false,
      places: props.places,
      apid: props.apid,
      comercialAll: [
        {
          title: 'Реклама 1',
          id: 1111,
          duration: '0:22'
        }, {
          title: 'Реклама 2',
          id: 2222,
          duration: '0:31'
        }, {
          title: 'Реклама 3',
          id: 3333,
          duration: '0:17'
        }
      ],
      comercialSettings: [
        // [
        //   {
        //     id: 1111,
        //     time: '12:23',
        //     src: ''
        //   }, {
        //     id: 1111,
        //     time: '11:17',
        //     src: ''
        //   }, {
        //     id: 2222,
        //     time: '17:28',
        //     src: ''
        //   }, {
        //     id: 3333,
        //     time: '22:00',
        //     src: ''
        //   }
        // ],
        // [
        //   {
        //     id: 1111,
        //     time: '12:23',
        //     src: ''
        //
        //   }, {
        //     id: 1111,
        //     time: '11:17',
        //     src: ''
        //   }, {
        //     id: 2222,
        //     time: '17:28',
        //     src: ''
        //   }, {
        //     id: 3333,
        //     time: '22:00',
        //     src: ''
        //   }
        // ]
      ],
      placeSettings: {},
      placeSettingsTemp: {},
    }
  }

  componentDidMount() {
    this.props.getPlaceSettings();
  }

  componentWillReceiveProps(props) {
    this.setState({'places': props.places, 'apid': props.apid, 'placeSettings': props.placeSettings, placeSettingsTemp: JSON.parse(JSON.stringify(props.placeSettings)) });
  }

  timesChange = (times) => {
    let placeSettings = this.state.placeSettings;
    let apid=this.state.apid;
    placeSettings[apid].times = times;
    this.setState({'placeSettings': placeSettings});
    this.throtleTimes(times,apid);
  }

  throtleTimes = (times, apid) => {
      clearTimeout(this.throttleTimesFunc);
      this.throttleTimesFunc = setTimeout(this.setTimesOnServer, 800, times, apid);
  };

  throttleTimesFunc = undefined;

  xhr = () => {
      let XHR = ("onload" in new XMLHttpRequest())
          ? XMLHttpRequest
          : XDomainRequest;
      let xhr = new XHR();
      xhr.withCredentials = true;
      return xhr
  }

  setTimesOnServer = (times, apid) => {
      let xhr=this.xhr();
      xhr.open("POST", "https://betacab.gusli.net/api/v1/place/"+apid+"/setTimes", true);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.onerror = function () {
          // debugger
          // alert('Ошибка ' + this.response);
      };
      xhr.onload = function () {
          if (this.response !== "{\"status\":\"ok\",\"affected\":true}") {
              // debugger
              // alert('Ошибка ' + this.response);
          }
      };
      xhr.send(JSON.stringify({'times': times}));
  };

  changeType = (event, index, value) => {
    let placeSettings = this.state.placeSettings;
    placeSettings[this.state.apid].type[0] = value;
    this.setState({'placeSettingsTemp': placeSettings});
  }

  textChange = (e, v, key) => {
    let placeSettings = this.state.placeSettingsTemp;
    if (key.match("phones")) {
      placeSettings[this.state.apid].phones[key.slice(6) * 1] = v;
    } else {
      placeSettings[this.state.apid][key] = v;
    }
    this.setState({'placeSettingsTemp': placeSettings});
  }

  // weekChange = (start, end, days, dayOn, key) => {
  //   let placeSettings = this.state.placeSettings;
  //   let apid = this.state.apid;
  //   if (dayOn !== undefined) {
  //     placeSettings[apid].days.forEach(function(item, i) {
  //       placeSettings[apid].days[i].days = placeSettings[apid].days[i].days.filter(function(day) {
  //         return day !== dayOn
  //       });
  //     });
  //   }
  //   placeSettings[apid].days[key] = {
  //     'start': start,
  //     'end': end,
  //     'days': days
  //   };
  //   this.setState({'placeSettings': placeSettings});
  // }

  // delWeek = (key) => {
  //   let placeSettings = this.state.placeSettings;
  //   let apid = this.state.apid;
  //   placeSettings[apid].days = placeSettings[apid].days.filter(function(item, i) {
  //     return i !== key
  //   })
  //   this.setState({'placeSettings': placeSettings});
  // }

  // plusWeek = () => {
  //   let placeSettings = this.state.placeSettings;
  //   let apid = this.state.apid;
  //   if (placeSettings[apid].days.length < 5) {
  //     let day = {
  //       days: [],
  //       start: 0,
  //       end: 1440
  //     }
  //     placeSettings[apid].days.splice(1, 0, day);
  //     this.setState({'placeSettings': placeSettings});
  //   }
  // }

  // pwdVisTog = () => {
  //   this.setState({
  //     'pwdVis': !this.state.pwdVis
  //   });
  // }

  // delLine = (key) => {
  //   let placeSettings = this.state.placeSettings;
  //   placeSettings[this.state.apid].staff.splice(key, 1);
  //   this.setState({'placeSettings': placeSettings});
  // }

  // plusLine = () => {
  //   let placeSettings = this.state.placeSettings;
  //   let apid = this.state.apid;
  //   if (placeSettings[apid].staff.length < 10) {
  //     placeSettings[apid].staff.push({"phone": '', "free": false})
  //     this.setState({'placeSettings': placeSettings});
  //   }
  // }

  staffChange = (v, type, index) => {
    let placeSettings = this.state.placeSettings;
    let apid = this.state.apid;
    if (type === "phone") { // && phone validation
      placeSettings[apid].staff[index].phone = v;
    }
    if (type === "free") {
      placeSettings[apid].staff[index].free = !placeSettings[apid].staff[index].free;
    }
    this.setState({'placeSettings': placeSettings});
  }

  // delComercial = (i) => {
  //   let comercialSettings = this.state.comercialSettings;
  //   let apid = this.state.apid;
  //   comercialSettings[apid].splice(i, 1)
  //   this.setState({'comercialSettings': comercialSettings});
  // }

  // changeComTime = (v, key, i) => {
  //   if (!v.match(/[^0-9]/)) {
  //     let comercialSettings = this.state.comercialSettings;
  //     let apid = this.state.apid;
  //     let time = comercialSettings[apid][i].time;
  //     v = v.length < 2
  //       ? ("0" + v)
  //       : v;
  //     v = v.slice(-2);
  //     if ((key === 0 && v * 1 <= 24) || (key === 1 && v * 1 < 60)) {
  //       let t = time.split(':');
  //       if (key === 0 && v === "24") {
  //         t[1] = '00';
  //       }
  //       if (key === 1 && t[0] !== "24") {
  //         t[key] = v;
  //         comercialSettings[apid][i].time = t.join(':');
  //         this.setState({'comercialSettings': comercialSettings});
  //       }
  //       if (key === 0) {
  //         t[key] = v;
  //         comercialSettings[apid][i].time = t.join(':');
  //         this.setState({'comercialSettings': comercialSettings});
  //       }
  //     }
  //   }
  // }

  // changeCom = (e, value, i) => {
  //   let comercialSettings = this.state.comercialSettings;
  //   let apid = this.state.apid;
  //   comercialSettings[apid][i].id = this.state.comercialAll[value].id;
  //   this.setState({'comercialSettings': comercialSettings});
  // }

  // plusComercial = () => {
  //   let comercialSettings = this.state.comercialSettings;
  //   let apid = this.state.apid;
  //   comercialSettings[apid].push({id: this.state.comercialAll[0].id, time: "", src: ""});
  //   this.setState({'comercialSettings': comercialSettings});
  // }

  changeReport = (v) => {
    let placeSettings=this.state.placeSettings;
    let apid=this.state.apid;
    placeSettings[apid].report=v;
    this.setState({'placeSettings':placeSettings});
  }

  resetSettings = () => {
    this.setState({'placeSettingsTemp': JSON.parse(JSON.stringify(this.state.placeSettings))});
  }

  submitSettings = () => {
    //проверка данных
    if (true) {
      let t=this
      let apid=this.state.apid;
      let xhr=this.xhr();
      xhr.open("POST", "https://betacab.gusli.net/api/v1/place/"+apid+"/settings", true);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.onerror = function () {
          // debugger
          // alert('Ошибка ' + this.response);
      };
      xhr.onload = function () {
          let data=JSON.parse(this.response);
          if (data && !data.data) {
              // debugger
              // alert('Ошибка ' + this.response);
          } else {
            let placeSettings=t.state.placeSettings;
            let placeSettingsTemp=t.state.placeSettingsTemp;

            data = data.data;

            let regdate = new Date(data.regdate);
            let month = regdate.getMonth();
            let year = regdate.getFullYear();
            const months = [
                "январь",
                "февраль",
                "март",
                "апрель",
                "май",
                "июнь",
                "июль",
                "август",
                "сентябрь",
                "октябрь",
                "ноябрь",
                "декабрь",
            ];
            const today = new Date();
            const tYear = today.getFullYear();
            const tMonth = today.getMonth();
            let opts = [];
            while (year < tYear || (year === tYear && month <= tMonth)) {
                opts.push({value: year + "-" + (month + 1), text: "за " + months[month] + " " + year});
                if (month === 11) {
                    month = 0;
                    year++;
                } else {
                    month++;
                }
            }
            data.opts = opts;
            data.report = opts[opts.length - 1].value;
            placeSettings[data.id] = data;
            placeSettingsTemp[data.id]=JSON.parse(JSON.stringify(data));
            t.setState({"placeSettings":placeSettings, "placeSettingsTemp":placeSettingsTemp});
          }
      };
      xhr.send(JSON.stringify({
        title: this.state.placeSettingsTemp[apid].title,
        type: this.state.placeSettingsTemp[apid].type,
        address: this.state.placeSettingsTemp[apid].address,
        place_phone: this.state.placeSettingsTemp[apid].phones[0]
      }));
    }
  }

  render() {
    const apid = this.state.apid;
    const place = this.state.placeSettingsTemp[this.state.apid];
    const opts = place?place.opts:[];
    let errors=["","","","",""];

    if (place && place.title.length===0 && !place.title.match(/[0-9a-b]/i)) { // Название
      errors[0]="Это поле должно быть заполнено";
    }

    if (place && place.title.length>255) { // Название
      errors[0]="Слишком длинное значение";
    }

    if (place && !place.type[0]) { // селект (тип)
      errors[1]="Это поле должно быть заполнено";
    }

    if (place && place.city.length===0) { // город
      errors[2]="Это поле должно быть заполнено";
    }

    if (place && place.city.length>255) { // город
      errors[2]="Слишком длинное значение";
    }

    if (place && place.address.length===0) { // адрес
      errors[3]="Это поле должно быть заполнено";
    }

    if (place && place.address.length>255) { // адрес
      errors[3]="Слишком длинное значение";
    }

    if (place && place.phones && !place.phones[0]) { // телефон
      errors[4]="Это поле должно быть заполнено";
    }

    if (place && place.phones && place.phones[0].length>20) { // телефон
      errors[4]="Слишком длинное значение";
    }

    //const comercial = this.state.comercialSettings[this.state.apid];
    //const place1=this.state.placeSettings;
    //const place1=this.state.placeSettings[this.state.apid];
    //const place = this.state.placeSettings[0]; //dev
    //const comercial = this.state.comercialSettings[0];
    return (
      !!(apid && place)?<div>
        {/*<Toolbar className="toolbar">
          <SelectField value={this.state.apid} onChange={(event, index, id) => this.props.changeApid(id)}>
            {this.state.places.map((row, index) => (<MenuItem key={index} value={row.id} primaryText={row.title}/>))}
          </SelectField>
        </Toolbar>*/}
        <Tabs >
          <Tab label="Редактировать заведение" value={0}>
            <TextField errorText={errors[0]} onChange={(e, v) => this.textChange(e, v, 'title')} value={place
              ? place.title
              : ''}/><br/>
            <SelectField  errorText={errors[1]} multiple={false}  value={place?place.type[0]:0} onChange={(e, index, value) => this.changeType(e, index, value)}>
                 {(place && place.available_types)?place.available_types.map((row, index) => (
                   <MenuItem key={index} value={row.id} primaryText={row.name}/>
                 )):''}
            </SelectField><br/>
            <TextField disabled value={place
              ? place.country
              : ''}/><br/>
            <TextField errorText={errors[2]} onChange={(e, v) => this.textChange(e, v, 'city')} value={place
              ? place.city
              : ''}/><br/>
            <TextField errorText={errors[3]} onChange={(e, v) => this.textChange(e, v, 'address')} value={place
              ? place.address
              : ''}/><br/>
            <TextField errorText={errors[4]} onChange={(e, v) => this.textChange(e, v, 'phones0')} value={place
              ? place.phones[0]
              : ''}/><br/>
            {/*<TextField onChange={(e, v) => this.textChange(e, v, 'phones1')} value={place
              ? place.phones[1]
              : ''}/><br/>*/}<br/>
            {/*<p>Часы работы</p>
            {place.days.map((row, index) => (<Week key={index} first={index === 0} days={row.days} start={row.start} end={row.end} onChange={(start, end, days, dayOn) => this.weekChange(start, end, days, dayOn, index)} delWeek={() => this.delWeek(index)} plusWeek={() => this.plusWeek()}/>))}
            <br/><br/>
            <p>Персонал</p>
            <div className="smallText">Редактировать управляющего</div>
            <TextField onChange={(e, v) => this.textChange(e, v, 'managerName')} value={place
              ? place.managerName
              : ''}/><br/>
            <TextField onChange={(e, v) => this.textChange(e, v, 'managerPhone')} value={place
              ? place.managerPhone
              : ''}/><br/>
            <TextField type={this.state.pwdVis
              ? 'text'
              : 'password'} onChange={(e, v) => this.textChange(e, v, 'managerPwd')} value={place
              ? place.managerPwd
              : ''}/>
            <FontIcon className={this.state.pwdVis
              ? 'fa fa-eye pwd'
              : 'fa fa-eye-slash pwd'} onClick={() => this.pwdVisTog()}/><br/>
            <br/><br/>
            <List className="staffList">
              <ListItem className='listItem'>
                <div className="col"><TextField onChange={(e, v) => this.textChange(e, v, 'managerPhone')} value={place
        ? place.managerPhone
        : ''}/></div>
                <div className="col">Управляющий</div>
              </ListItem>
              {place.staff.map((row, index) => (
                <ListItem className='listItem' key={index}>
                  <div className="col"><TextField onChange={(e, v) => this.staffChange(v, 'phone', index)} value={row.phone}/></div>
                  <div className="col"><Checkbox checked={row.free} onCheck={() => this.staffChange('', 'free', index)} label="Начислять бесплатные заказы"/></div>
                  <div className="col"><FontIcon className="fa fa-close" onClick={() => this.delLine(index)}/></div>
                </ListItem>
              ))}
              <ListItem className='listItem'>
                <div className="col"><TextField disabled={true}/></div>
                <div className="col"><Checkbox disabled={true}/>Добавить персонал</div>
                <div className="col"><FontIcon className="fa fa-plus" onClick={() => this.plusLine()}/></div>
              </ListItem>
            </List>*/}

            <RaisedButton label="Отмена" onClick={() => this.resetSettings()}/>
            <RaisedButton label="Сохранить"  onClick={() => this.submitSettings()}/>
          </Tab>
          <Tab label="Настройки плеера" value={1}>
            <Toggle label="Разрешить персоналу менять фоновую музыку в плеере GUSLI" labelPosition="left" defaultToggled={place.can_change_playlist} onToggle={() => this.props.togglePermissions(0)}/>
            <Toggle label="Разрешить персоналу включать локальную музыку в плеере GUSLI" labelPosition="left" defaultToggled={place.can_play_local} onToggle={() => this.props.togglePermissions(1)}/>
            <Toggle label="Разрешить персоналу блокировать музыку в плеере GUSLI" labelPosition="left" defaultToggled={place.can_block_muslic} onToggle={() => this.props.togglePermissions(2)}/><br/><br/>
            <p>Настройки времени суток</p>
            <div className="smallText">Для воспроизведения музыки в плеере</div>
            <Times times={place.times} change={(times) => this.timesChange(times)}/>
            <br/><br/>
            {/*<RaisedButton label="Отмена"/>
            <RaisedButton label="Сохранить"/>
            <RaisedButton label="Применить ко всем"/>*/}
          </Tab>
          {/*<Tab label="Настройки рекламы" value={2}>
            <p>Время запуска рекламы (локальное для заведений)</p>
            <Table displayRowCheckbox={false} deselectOnClickaway={false} className='comTable'>
              <TableHeader>
                <TableRow>
                  <TableHeaderColumn className="comercials">Время запуска</TableHeaderColumn>
                  <TableHeaderColumn className="comercials">Рекламный трек</TableHeaderColumn>
                  <TableHeaderColumn className="comercials">Действия</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody>
                {comercial.map((row, index) => (
                  <TableRow key={index} className="comercials" selectable={false}>
                    <TableRowColumn>
                      <div className="col">
                        <TextField value={row.time.split(':')[0]} onChange={(e, v) => this.changeComTime(v, 0, index)}/>:
                        <TextField value={row.time.split(':')[1]} onChange={(e, v) => this.changeComTime(v, 1, index)}/>
                      </div>
                    </TableRowColumn>
                    <TableRowColumn>
                      <div className="col">
                        <IconButton>
                          <FontIcon className="fa fa-play-circle"/>
                        </IconButton>
                      </div>
                      <div className="col">
                        <SelectField value={row.id} onChange={(e, value) => this.changeCom(e, value, index)}>
                          {this.state.comercialAll.map((row1, index1) => (<MenuItem key={index1} value={row1.id} primaryText={row1.title}/>))}
                        </SelectField>
                      </div>
                      <div className="col">
                        {this.state.comercialAll.filter(function(item) {
                          return item.id === row.id
                        })[0].duration}
                      </div>
                    </TableRowColumn>
                    <TableRowColumn>
                      <IconButton className="smallIc" onClick={() => this.delComercial(index)}>
                        <FontIcon className="fa fa-close"/>
                      </IconButton>
                    </TableRowColumn>
                  </TableRow>
                ))}

                <TableRow className="comercials ghost" selectable={false}>
                  <TableRowColumn>
                    <div className="col">
                      <TextField disabled={true}/>:
                      <TextField disabled={true}/>
                    </div>
                  </TableRowColumn>
                  <TableRowColumn>
                    <div className="col">
                      <IconButton disabled={true}>
                        <FontIcon className="fa fa-play-circle"/>
                      </IconButton>
                    </div>
                    <div className="col">
                      <SelectField disabled={true}/>
                    </div>
                    <div className="col"></div>
                  </TableRowColumn>
                  <TableRowColumn>
                    <IconButton className="smallIc" onClick={() => this.plusComercial()}>
                      <FontIcon className="fa fa-plus"/>
                    </IconButton>
                  </TableRowColumn>
                </TableRow>

              </TableBody>
            </Table>
            <RaisedButton label="Загрузить"/>
          </Tab>*/}
          <Tab label="Отчёты РОА и ВОИС">
            <SelectField value={place.report} onChange={(e,t,v) => this.changeReport(v)}>
            {opts?opts.map((row,index) => (
              <MenuItem
                key={index}
                value={row.value}
                primaryText={row.text}
              />
            )):''}
            </SelectField>
            <a href={'https://betacab.gusli.net/api/v1/place/'+apid+'/raoReport?month='+place.report} download><RaisedButton label="Скачать"/></a>
          </Tab>
        </Tabs>
      </div>:<RefreshIndicator
      size={50}
      left={70}
      top={0}
      loadingColor="#FF9800"
      status="loading"
      style={style.refresh}
    />
    );
  }
}

export default PlaceSettings;
