import React, {Component} from 'react';

// Material-UI
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
// Font
import 'typeface-roboto'

class Week extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      focus: [false, false, false, false]
    }
  }

  toggleDay(e,index) {
    let days = this.props.days;
    let dayOn;
    if (days.indexOf(index) === -1) {
      days.push(index);
      dayOn=index;
    } else {
      days = days.filter(function(item) {
        return item !== index
      });
    }
    this.props.onChange(this.props.start, this.props.end, days, dayOn);
    //this.setState({'days': days});
  }

  timeChange = (e, v, t, p) => {
    if (!v.match(/[^0-9]/)) {
      let time = this.getTime();
      let i = p * 2 + t;
      if (t === 0 && v*1 === 24) {
        time[i + 1] = '00';
      }
      if (!(t === 1 && time[i - 1] === 24)) {
        if (v.length === 1) {
          v = '0' + v;
        }
        if (v.length > 2) {
          v.slice(0, 2);
        }
        if ((t === 0 && v > 24) || (t === 1 && v > 59)) {
          v = time[i];
        }

        time[i] = v;
        let start = time[0] * 60 + time[1] * 1;
        let end = time[2] * 60 + time[3] * 1;
        this.props.onChange(start, end, this.props.days);
        //this.setState({'time': time, 'start': start, 'end': end});
      }
    }
  }

  getTime = () => {
    let start = this.props.start;
    let end = this.props.end;
    let arr = [
      Math.floor(start / 60) + '',
      start % 60 + '',
      Math.floor(end / 60) + '',
      end % 60 + ''
    ];
    arr.forEach(function(item, i) {
      if (item.length === 1) {
        arr[i] = '0' + item;
      }
    });
    return arr
  }

  blurCheckerT = () => {}

  blurChecker = () => {
    let focus = this.state.focus;
    if (focus.indexOf(true) === -1) {
      let start = this.props.start;
      let end = this.props.end;
      if (start > end) { //перевертыш
        let temp = end;
        end = start;
        start = temp;
        this.props.onChange(start, end, this.props.days);
        //this.setState({'start':start,'end':end,'time':time});
      }
    }
    this.setState({'focus': focus});
  }

  blur = (i) => {
    let focus = this.state.focus;
    focus[i] = false;
    this.blurCheckerT = setTimeout(this.blurChecker.bind(this), 200);
  }

  focus = (i) => {
    clearTimeout(this.blurCheckerT);
    let focus = this.state.focus;
    focus[i] = true;
    this.setState({'focus': focus});
  }

  h24 = () => {
    this.props.onChange(0, 1440, this.props.days);
  }

  render() {
    //const state = this.state;
    const week = [
      'Пн',
      'Вт',
      'Ср',
      'Чт',
      'Пт',
      'Сб',
      'Вс'
    ];
    let days = this.props.days;
    let time = this.getTime();

    return (
      <div className={this.props.first?"week first":"week"}>
        <div>
          {week.map((row, index) => (
            <div key={index} className={(days && days.indexOf(index) > -1)
              ? 'day active'
              : 'day'} onClick={(e) => this.toggleDay(e,index)}>{row}</div>
          ))}
        </div>
        <div className="h24" onClick={() => this.h24()}>24 ч</div>
        <TextField value={time[0]} className="time" onChange={(e, v) => this.timeChange(e, v, 0, 0)} onBlur={() => this.blur(0)} onFocus={() => this.focus(0)}/>:
        <TextField value={time[1]} className="time" onChange={(e, v) => this.timeChange(e, v, 1, 0)} onBlur={() => this.blur(1)} onFocus={() => this.focus(1)}/>
        <div className="divider">-</div>
        <TextField value={time[2]} className="time" onChange={(e, v) => this.timeChange(e, v, 0, 1)} onBlur={() => this.blur(2)} onFocus={() => this.focus(2)}/>:
        <TextField value={time[3]} className="time" onChange={(e, v) => this.timeChange(e, v, 1, 1)} onBlur={() => this.blur(3)} onFocus={() => this.focus(3)}/>

        <IconButton onClick={() => this.props.delWeek()} className="delWeek tooltip" tooltip="Удалить интервал" tooltipPosition="top-right">
          <FontIcon className="fa fa-close"/>
        </IconButton>
        <IconButton onClick={() => this.props.plusWeek()} className="plusWeek tooltip" tooltip="Добавить интервал" tooltipPosition="top-right">
          <FontIcon className="fa fa-plus"/>
        </IconButton>
      </div>
    );
  }
}

export default Week;
