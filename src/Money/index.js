import React, {Component} from 'react';

// Material-UI
//import {Tabs, Tab} from 'material-ui/Tabs';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';
import RefreshIndicator from 'material-ui/RefreshIndicator';

// Font
import 'typeface-roboto'

const style = {
  container: {
    position: 'relative',
  },
  refresh: {
    display: 'inline-block',
    position: 'relative',
  },
};

class Money extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      payouts: this.props.payouts,
      num: this.props.num,
      total: this.props.total,
      currency: this.props.currency,
      method: 0,
      refs: this.props.refs,
      refLink: this.props.refLink,
      text: ''
    }
  }

  componentDidMount() {
    this.props.getPayouts();
  }

  changeMethod = (value) => {
    let obj={method: value}
    let payouts=this.state.payouts;
    let req=payouts.requisites
    if (req && req.type && req.type==value) {
      obj.text=req.value;
    } else {
      obj.text='';
    }
    this.setState(obj);
  }

  summ = () => { //вынести на получение
    let refs=this.state.refs||[];
    let s = 0;
    refs.forEach(function(item) {
      s += item.earned
    });
    return s
  }

  componentWillReceiveProps(props) {
    let text='';
    if (props.payouts && props.payouts.requisites && props.payouts.requisites.type && props.payoutMethod && props.payoutMethod==props.payouts.requisites.type) {
      text=props.payouts.requisites.value;
    }
    this.setState({'refLink':'https://vk.com','refs': props.refs, 'currency': props.currency, 'num': props.num, 'total': props.total, 'payouts': props.payouts, method: props.payoutMethod, text: text});
  }

  copyLink = (e) => {
    e.target.select();
    try {
      let successful = document.execCommand('copy');
      let msg = successful
        ? 'successful'
        : 'unsuccessful';
      console.log('Copying text command was ' + msg);
    } catch (err) {
      console.log('Oops, unable to copy');
    }
  }

  changeReq = (v) => {
    this.setState({'text':v})
  }

  submit = () => {
    let t=this;
    let type=this.state.method;
    let value=this.state.text;
    if (value && value!=='') {
      let XHR = ("onload" in new XMLHttpRequest())
          ? XMLHttpRequest
          : XDomainRequest;
      let xhr = new XHR();
      xhr.withCredentials = true;
      xhr.open("POST", "https://betacab.gusli.net/api/v1/payout", true);
      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      xhr.onload = function () {
          if (JSON.parse(this.responseText).status_code === 401) {
              t.logout();
          } else { //this.response==='{"status":"ok"}'
              t.props.setNum();
          }
      };
      xhr.onerror = function () {
          debugger;
          //alert('Ошибка ' + this.status);
      };
      xhr.send('type='+type+'&value='+value);
      //}
    }
  }

  render() {
    let payouts=this.state.payouts;
    //let refs=this.state.refs||[];
    //let method=this.state.method;
    let methods=[];
    let payoutTypes=this.state.payouts.found_payout_types;
    for (let key in payoutTypes) {
      methods.push({value: key, text: payoutTypes[key]});
    }
    let avalible=this.state.num>payouts.minimalPayout;
    return (
      !!(payouts && payouts.history)?<div>
        {/*<Tabs className="payoutTabs">
          <Tab label="Вывод средств" value="a">*/}

              {avalible?
                <div className="col">
                <SelectField value={this.state.method+''} onChange={(e, index, value) => this.changeMethod(value)}>
                  {methods.map((row, index) => (
                    <MenuItem key={index} value={row.value} primaryText={row.text}/>
                  ))}
                </SelectField><br/>
                <TextField hintText={'Введите платежные данные'} value={this.state.text} onChange={(e,v)=>this.changeReq(v)}/><br/>
                <RaisedButton label="Вывести" onClick={() => this.submit()}/>
                </div>
              :<div className="col sorry"><p>Извините, ваш баланс меньше минимальной суммы вывода</p></div>}


            <div className="col">
              {/*<p>Всего заработано: {this.state.total} {this.state.currency}</p>*/}
              <p>Баланс: {Math.floor(this.state.num/100)} {this.state.currency}</p>
            </div>
            <br/><br/>
            <p>История выплат</p>
            <Table selectable={false}>
              <TableHeader>
                <TableRow>
                  <TableHeaderColumn>Дата</TableHeaderColumn>
                  <TableHeaderColumn>Сумма {this.state.currency}</TableHeaderColumn>
                  <TableHeaderColumn>Реквизиты</TableHeaderColumn>
                  <TableHeaderColumn>Статус</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody>
                {payouts.history.map((row, index) => (
                  <TableRow key={index}>
                    <TableRowColumn>{row.created_at}</TableRowColumn>
                    <TableRowColumn>{row.sum}</TableRowColumn>
                    <TableRowColumn>
                      {row.address}
                      <div className="subText grey">{row.requsites}</div>
                    </TableRowColumn>
                    <TableRowColumn>{row.status_title}</TableRowColumn>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          {/*</Tab>
          <Tab label="Реферальная программа" value="b">
            <div className="col">
              <b>Ваша ссылка</b><br/>
              <div>Нажмите на неё, чтобы скопировать</div>
              <TextField className="refLink" value={this.state.refLink} onClick={(e) => this.copyLink(e)}/><br/>
              <p>Вы можете получать дополнительный доход с заведений, подключённых по вашей рекомендации. Для этого вам нужно поделиться ссылкой, которая расположена выше.</p><br/>
            </div>
            <p>Подключенные заведения</p>
            <Table selectable={false}>
              <TableHeader>
                <TableRow>
                  <TableHeaderColumn>Заведение</TableHeaderColumn>
                  <TableHeaderColumn>Заработано {this.state.currency}</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody>
                {refs.map((row, index) => (
                  <TableRow key={index} className='ref'>
                    <TableRowColumn>
                      <div className="col"><img src={""}/></div>
                      <div className="col">
                        <div>{row.title}</div>
                        <div className="subText">{row.address}</div>
                      </div>
                    </TableRowColumn>
                    <TableRowColumn>{row.earned}</TableRowColumn>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
            <p className="right medText">
              <b>Всего заработано: {this.summ()} {this.state.currency}</b>
            </p>
          </Tab>
        </Tabs>*/}
      </div>:<RefreshIndicator
      size={50}
      left={70}
      top={0}
      loadingColor="#FF9800"
      status="loading"
      style={style.refresh}
    />
    );
  }
}

export default Money;
