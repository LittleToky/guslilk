import React, {Component} from "react";
import QueryString from "query-string";
import {Cookies, withCookies} from "react-cookie";

// styles
import "./App.css";
import "./font-awesome/css/font-awesome.min.css";

// App
import Music from "./Music";
import Filter from "./Filter";
import Popular from "./Popular";
import Places from "./Places";
import PlaceSettings from "./PlaceSettings";
import Profile from "./Profile";
import Money from "./Money";
import Help from "./Help";
import Stats from "./Stats";
import EditPlaylist from "./EditPlaylist";
import Login from "./Login";
import Registration from "./Registration";

// Material-UI
import {deepOrange500} from "material-ui/styles/colors";
import {List, ListItem} from "material-ui/List";
import {Toolbar, ToolbarGroup} from "material-ui/Toolbar";
import RaisedButton from "material-ui/RaisedButton";
import IconButton from "material-ui/IconButton";
import FontIcon from "material-ui/FontIcon";
import Popover from "material-ui/Popover";
import Menu from "material-ui/Menu";
import MenuItem from "material-ui/MenuItem";
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

// Theme
import getMuiTheme from "material-ui/styles/getMuiTheme";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
// Font
import "typeface-roboto";

// Click handler
import injectTapEventPlugin from "react-tap-event-plugin";
import ReactDOM from "react-dom";

injectTapEventPlugin();

// Theme
const muiTheme = getMuiTheme({
    palette: {
        accent1Color: deepOrange500,
    },
});

const initialState = JSON.stringify({
    apid: "",
    owner: {},
    balance: "",
    num: "",
    currency: "",
    chapter: "Profile",
    placeStats: {},
    artists: {},
    genres: {},
    playListsAll: [],
    playlistGenres: [],
    playListSettings: {},
    popular: {},
    payouts: {},
    songs: {},
    places: [],
    refLink: "",
    blocked: {},
    placeSettings: {},
    otherPlaylists: {},
    activePlaylist: false,
    playlistBank: {}, // для EditPlaylist
    userMenu: {
        open: false,
        anchorEl: null,
    },
    login: false,
    loadDialog: false,
    payoutMethod: '0'
});

class App extends Component {

    constructor(props, context) {

        super(props, context);
        this.showTrans = function () {
            console.log("actionFromComp");
        };
        this.state = JSON.parse(initialState);
        this.cookies = new Cookies();

    }

    // changeApid = (apid) => {
    //     this.setState({
    //         "apid": apid,
    //     }, function () {
    //         switch (this.state.chapter) {
    //             case "Music":
    //                 this.getPlayListSettings();
    //                 break;
    //
    //             case "Filter":
    //                 this.getFilterGenres();
    //                 this.getFilterArtists();
    //                 this.getFilterSongs();
    //                 break;
    //
    //             case "Popular":
    //                 this.getPopular();
    //                 break;
    //
    //             // case "PlaceSettings": // проверить потом, нужно ли
    //             //   debugger;
    //             //break;
    //
    //             case "EditPlaylist":
    //                 this.getPlayListSettings(); // да, как в Music - это не ошибка (getPlaylist Выполняется внутри)
    //                 break;
    //
    //             case "Stats":
    //                 this.getPlaceStats();
    //                 break;
    //
    //             default:
    //                 return "";
    //         }
    //     });
    // }

    componentDidMount() {
        this.askIfLog();
    }

    ////////////////////////////////////////////

    xhr = () => {
        let XHR = ("onload" in new XMLHttpRequest())
            ? XMLHttpRequest
            : XDomainRequest;
        let xhr = new XHR();
        xhr.withCredentials = true;
        return xhr
    }

    askIfLog = () => {
        this.tryLoginByToken();
        let t = this;
        let xhr = this.xhr();
        xhr.open("GET", "https://betacab.gusli.net/api/v1/owner", true);

        xhr.onload = function () {
            if (JSON.parse(this.responseText).status_code === 401) {
                    window.location.replace('https://betacab.gusli.net/logout')
            } else {
                let data = JSON.parse(this.response).data;
                if (data) {
                    data.name = data.owner;
                    delete(data.owner);
                    t.setState({"owner": data, "currency": data.currency, "num": data.balance, "login": true});
                    t.getPlaces();
                    t.getPlayListsAll();
                }
            }
        };
        xhr.onerror = function () {
            //debugger
            //alert('Ошибка ' + this.status);
        };
        if(!this.state.login) {
            xhr.send();
        }
    };

    login = async (phone, pwd) => { //Выполняется первым и логинится
        const ft = await fetch("https://betacab.gusli.net/api/v1/auth/login", {
            credentials: "include",
            method: "post",
            body: "phone=" + phone + "&password=" + pwd,
            headers: {
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
            },
        });
        let data = await ft.json();
        if (!data.hasOwnProperty("error")) {
            data = data.data;
            data.name = data.owner;
            delete(data.owner);
            this.setState({"owner": data, "currency": data.currency, "num": data.balance, "login": true});
            console.log("%c login", "color: #090; font-size: 16px");
            this.getPlaces();
            this.getPlayListsAll();
        }
    };
    registration = async (phone, email) => {
        const ft = await fetch("https://betacab.gusli.net/api/v1/auth/login", {
            credentials: "include",
            method: "post",
            body: "phone=" + phone + "&password=" + email,
            headers: {
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
            },
        });
        let data = await ft.json();
        if (!data.error) {
            data = data.data;
            data.name = data.owner;
            delete(data.owner);
            this.setState({"owner": data, "currency": data.currency, "num": data.balance, "login": true});
            console.log("%c login", "color: #090; font-size: 16px");
            this.getPlaces();
            this.getPlayListsAll();
        }
    };

    getPlayListsAll = () => {
        let t = this;
        let xhr = this.xhr();
        xhr.open("GET", "https://betacab.gusli.net/api/v1/playlists", true);
        xhr.onload = function () {
            if (JSON.parse(this.responseText).status_code === 401) {
                t.logout();
            } else {
                let playListsAll = JSON.parse(this.responseText).data;
                let playlistGenres = [];
                playListsAll.forEach(function (pl) {
                    pl.genres.forEach(function (gen) {
                        if (playlistGenres.indexOf(gen) === -1) {
                            playlistGenres.push(gen);
                        }
                    });
                });
                playlistGenres = playlistGenres.map(function (item) {
                    return {title: item, visible: true};
                }, []);
                t.setState({
                    "playListsAll": playListsAll,
                    "playlistGenres": playlistGenres,
                    "activePlaylist": playListsAll[0].id,
                });
                console.log("%c playListsAll", "color: #090; font-size: 16px");
            }
        };
        xhr.onerror = function () {
            debugger;
            //alert('Ошибка ' + this.status);
        };
        xhr.send();
    };

    getPlaces = () => {
        let t = this;
        let xhr = this.xhr();
        xhr.open("GET", "https://betacab.gusli.net/api/v1/places", true);
        xhr.onload = function () {
            if (JSON.parse(this.responseText).status_code === 401) {
                t.logout();
            } else {
                let places = JSON.parse(this.responseText).data;
                t.setState({"places": places, "apid": places[0].id});
                console.log("%c places", "color: #090; font-size: 16px");
            }
        };
        xhr.onerror = function () {
            debugger;
            //alert('Ошибка ' + this.status);
        };
        xhr.send();
    };

    getPlayListSettings() { //для music
        let apid = this.state.apid;
        // if (!this.state.playListSettings[apid]) {
        let t = this;
        let xhr = this.xhr();
        xhr.open("GET", "https://betacab.gusli.net/api/v1/music/" + apid, true);
        xhr.onload = function () {
            if (JSON.parse(this.responseText).status_code === 401) {
                t.logout();
            } else {
                let data = JSON.parse(this.responseText).data;
                let apid = t.state.apid;
                let playListSettings = t.state.playListSettings;
                let otherPlaylists = t.state.otherPlaylists;
                let all = t.state.playListsAll;
                otherPlaylists[apid] = {
                    morning: [],
                    day: [],
                    evening: [],
                };
                playListSettings[apid] = {
                    morning: [],
                    day: [],
                    evening: [],
                };
                for (let key in data) {
                    let ids = data[key].map(function (item) {
                        return item.id;
                    }, []);
                    all.forEach(function (item) {
                        if (ids.indexOf(item.id) === -1) {
                            item.visible = true;
                            otherPlaylists[apid][key].push(item);
                        } else {
                            let elem = data[key].filter(function (it) {
                                return it.id === item.id;
                            })[0];
                            elem.title = item.title;
                            elem.genres = item.genres;
                            elem.visible = true;
                            playListSettings[apid][key].push(elem);
                        }
                    });
                }
                t.setState({
                    "playListSettings": playListSettings,
                    "otherPlaylists": otherPlaylists,
                }, function () {
                    this.getPlaylist();
                });
                console.log("%c playListSettings otherPlaylists for apid " + apid, "color: #090; font-size: 16px");
            }
        };
        xhr.onerror = function () {
            debugger;
            //alert('Ошибка ' + this.status);
        };
        xhr.send();
        //this.getPlaylist();
        // } else {
        //   this.getPlaylist();
        // }
    }

    getPlaceStats = () => {
        let apid = this.state.apid;
        //if (apid) {
        //if (!this.state.placeStats[apid]) {
        let t = this;
        let xhr = this.xhr();
        xhr.open("GET", "https://betacab.gusli.net/api/v1/places/stat/" + apid, true);
        xhr.onload = function () {
            if (JSON.parse(this.responseText).status_code === 401) {
                t.logout();
            } else {
                let data = JSON.parse(this.responseText).data;
                let placeStats = t.state.placeStats;
                ["today", "yesterday", "month"].forEach(function (key) {
                    data[key].forEach(function (item, i) {
                        data[key][i].duration = t.durationFormater(data[key][i].duration);
                    });
                });
                placeStats[apid] = data;
                t.setState({"placeStats": placeStats});
                console.log("%c placeStats for apid " + apid, "color: #090; font-size: 16px");
            }
        };
        xhr.onerror = function () {
            debugger;
            //alert('Ошибка ' + this.status);
        };
        xhr.send();
        //}
        // } else {
        //   debugger
        // }
    };

    getPopular = () => {
        let apid = this.state.apid;
        //if (apid) {
        //if (!this.state.popular[apid]) {
        let t = this;
        let xhr = this.xhr();
        xhr.open("GET", "https://betacab.gusli.net/api/v1/popular/" + apid, true);
        xhr.onload = function () {
            if (JSON.parse(this.responseText).status_code === 401) {
                t.logout();
            } else {
                let data = JSON.parse(this.responseText).data;
                let popular = t.state.popular;
                data.forEach(function (item, i) {
                    data[i].duration = t.durationFormater(item.duration);
                });
                popular[apid] = data;
                t.setState({"popular": popular});
                console.log("%c popular for apid " + apid, "color: #090; font-size: 16px");
            }
        };
        xhr.onerror = function () {
            debugger;
            //alert('Ошибка ' + this.status);
        };
        xhr.send();
    };

    getFilterSongs = () => {
        let apid = this.state.apid;
        //if (!this.state.songs[apid]) {
        let t = this;
        let xhr = this.xhr();
        xhr.open("GET", "https://betacab.gusli.net/api/v1/filter/songs/" + apid, true);
        xhr.onload = function () {
            if (JSON.parse(this.responseText).status_code === 401) {
                t.logout();
            } else {
                let data = JSON.parse(this.responseText).data;
                let songs = t.state.songs;
                data.forEach(function (item, i) {
                    data[i].visible = true;
                    data[i].duration = t.durationFormater(item.duration);
                });
                songs[apid] = data;
                t.setState({"songs": songs});
                console.log("%c filterSongs for apid " + apid, "color: #090; font-size: 16px");
            }
        };
        xhr.onerror = function () {
            alert("Ошибка " + this.status);
        };
        xhr.send();
        //}
    };

    getFilterArtists = () => {
        let apid = this.state.apid;
        //if (!this.state.artists[apid]) {
        let t = this;
        let xhr = this.xhr();
        xhr.open("GET", "https://betacab.gusli.net/api/v1/filter/artists/" + apid, true);
        xhr.onload = function () {
            if (JSON.parse(this.responseText).status_code === 401) {
                t.logout();
            } else {
                let data = JSON.parse(this.responseText).data;
                let artists = t.state.artists;
                data.forEach(function (item, i) {
                    data[i].visible = true;
                });
                artists[apid] = data;
                t.setState({"artists": artists});
                console.log("%c filterArtists for apid " + apid, "color: #090; font-size: 16px");
            }
        };
        xhr.onerror = function () {
            debugger;
            //alert('Ошибка ' + this.status);
        };
        xhr.send();
        //}
    };

    getFilterGenres = () => {
        let apid = this.state.apid;
        //if (!this.state.genres[apid]) {
        let t = this;
        let xhr = this.xhr();
        xhr.open("GET", "https://betacab.gusli.net/api/v1/filter/genres/" + apid, true);
        xhr.onload = function () {
            if (JSON.parse(this.responseText).status_code === 401) {
                t.logout();
            } else {
                let data = JSON.parse(this.responseText).data;
                let genres = t.state.genres;
                data.forEach(function (item, i) {
                    data[i].visible = true;
                });
                genres[apid] = data;
                t.setState({"genres": genres});
                console.log("%c filterGenres for apid " + apid, "color: #090; font-size: 16px");
            }
        };
        xhr.onerror = function () {
            debugger;
            //alert('Ошибка ' + this.status);
        };
        xhr.send();
        //}
    };

    getPlaylist = () => {
        this.getBlocked();
        let apid = this.state.apid;
        let id = this.state.activePlaylist;
        let playlistBank = this.state.playlistBank;
        let playListSettings = this.state.playListSettings;
        let t = this;
        //if (!playlistBank[id]) {
        let xhr = this.xhr();
        xhr.open("GET", "https://betacab.gusli.net/api/v1/playlist/" + id, true);
        xhr.onload = function () {
            if (JSON.parse(this.responseText).status_code === 401) {
                t.logout();
            } else {
                let data = JSON.parse(this.responseText).data;
                let playListsAll = t.state.playListsAll;
                let duration = 0;
                data.reduce(function (prev, item) {
                    duration += item.duration;
                }, duration);
                duration = Math.floor(duration / 1000);
                let d = [];
                let h = Math.floor(duration / 3600);
                if (h > 0) {
                    d.push(h);
                }

                duration = t.durationFormater(duration % 3600).split(":");
                d = d.concat([duration[0], duration[1]]);
                if (d[1].length === 0) {
                    d[1] = "00";
                } else if (d[1].length === 1) {
                    d[1] = "0" + d[1];
                }
                let current = playListsAll.filter(function (item) {
                    return item.id === id;
                })[0];
                data.forEach(function (item, i) {
                    let s = Math.floor(item.duration / 1000);
                    data[i].duration = t.durationFormater(s);
                });
                playlistBank[id] = {
                    title: current.title,
                    tracks: data,
                    num: data.length,
                    duration: d.join(":"),
                    diy: current.diy,
                    url: current.url // картинка
                };
                t.setState({
                    "playlistBank": playlistBank,
                }, function () {
                    this.mergePlacePlaylist(t, playlistBank, id, apid, playListSettings);
                });
                console.log("%c playlistBank for plid " + id, "color: #090; font-size: 16px");
            }
        };
        xhr.onerror = function () {
            debugger;
            //alert('Ошибка ' + this.status);
        };
        xhr.send();
    };

    getBlocked = () => { //получение настроек для EditPlaylist
        let apid = this.state.apid;
        //if (!this.state.blocked[apid]) {
        let t = this;
        let xhr = this.xhr();
        xhr.open("GET", "https://betacab.gusli.net/api/v1/place/" + apid + "/getBlockedTracks", true);
        xhr.onload = function () {
            if (JSON.parse(this.responseText).status_code === 401) {
                t.logout();
            } else {
                let data = JSON.parse(this.responseText).data;
                let blocked = t.state.blocked;
                blocked[apid] = data;
                t.setState({"blocked": blocked});
                console.log("%c Blocked for apid " + apid, "color: #090; font-size: 16px");
            }
        };
        xhr.onerror = function () {
            debugger;
            //alert('Ошибка ' + this.status);
        };
        xhr.send();
        //}
    };

    getPlaceSettings = () => {
        let apid = this.state.apid;
        let t = this;
        let xhr = this.xhr();
        xhr.open("GET", "https://betacab.gusli.net/api/v1/place/" + apid + "/settings", true);
        xhr.onload = function () {
            if (JSON.parse(this.responseText).status_code === 401) {
                t.logout();
            } else {
                let data = JSON.parse(this.responseText).data;
                let regdate = new Date(data.regdate);
                let month = regdate.getMonth();
                let year = regdate.getFullYear();
                const months = [
                    "январь",
                    "февраль",
                    "март",
                    "апрель",
                    "май",
                    "июнь",
                    "июль",
                    "август",
                    "сентябрь",
                    "октябрь",
                    "ноябрь",
                    "декабрь",
                ];
                const today = new Date();
                const tYear = today.getFullYear();
                const tMonth = today.getMonth();
                let opts = [];
                while (year < tYear || (year === tYear && month <= tMonth)) {
                    opts.push({value: year + "-" + (month + 1), text: "за " + months[month] + " " + year});
                    if (month === 11) {
                        month = 0;
                        year++;
                    } else {
                        month++;
                    }
                }
                data.opts = opts;
                data.report = opts[opts.length - 1].value;
                let placeSettings = t.state.placeSettings;
                placeSettings[data.id] = data;
                t.setState({"placeSettings": placeSettings});
                console.log("%c PlaceSettings for apid " + apid, "color: #090; font-size: 16px");
            }
        };
        xhr.onerror = function () {
            debugger;
            //alert('Ошибка ' + this.status);
        };
        xhr.send();
    };

    getPayouts = () => {
        let t = this;
        let xhr = this.xhr();
        xhr.open("GET", "https://betacab.gusli.net/api/v1/payout", true);
        xhr.onload = function () {
            if (JSON.parse(this.responseText).status_code === 401) { //проверить
                t.logout();
            } else {
                let payouts = JSON.parse(this.responseText).data;
                payouts.history.forEach(function (item, i) {
                    payouts.history[i].sum = item.sum / 100;
                });
                let obj={"payouts": payouts};
                let req=payouts.requisites;
                if (req &&req.type) {
                  obj.payoutMethod=req.type;
                } else {
                  obj.payoutMethod=0;
                }
                t.setState(obj);
                console.log("%c Payouts", "color: #090; font-size: 16px");
            }
        };
        xhr.onerror = function () {
            debugger;
            //alert('Ошибка ' + this.status);
        };
        xhr.send();
    };

    ////////////////////////////////////////////

    mergePlacePlaylist = (t, playlistBank, id, apid, playListSettings) => {
        let settings = playListSettings[apid];
        let bank = playlistBank[id];
        if (playlistBank[id] && settings) {
            bank.selected = playlistBank[id].selected
                ? playlistBank[id].selected
                : {};
            bank.selected[apid] = [];
            if (settings.morning.filter(function (item) {
                    return item.id === id;
                }).length !== 0) {
                bank.selected[apid].push("утро");
            }
            if (settings.day.filter(function (item) {
                    return item.id === id;
                }).length !== 0) {
                bank.selected[apid].push("день");
            }
            if (settings.evening.filter(function (item) {
                    return item.id === id;
                }).length !== 0) {
                bank.selected[apid].push("вечер");
            }
            t.setState({"playlistBank": playlistBank});
        }
    };

    durationFormater = (s) => {
        let min = Math.floor(s / 60);
        let sec = s % 60;
        if (sec === 0) {
            sec = "00";
        } else if (sec < 10) {
            sec = "0" + sec;
        }
        return [min, sec].join(":");
    };

    setProb = (value, pl, time) => {
        let playListSettings = this.state.playListSettings;
        let apid = this.state.apid;
        let i = playListSettings[apid][time].indexOf(pl);
        playListSettings[apid][time][i].probability = value;
        this.setState({"playListSettings": playListSettings});
        this.throttleProb(value, pl, time, apid);
    };

    throttleProb = (value, pl, time, apid) => {
        clearTimeout(this.throttleProbFunc);
        this.throttleProbFunc = setTimeout(this.setProbOnServer, 500, value, pl, time, apid);
    };

    throttleProbFunc = undefined;

    setProbOnServer = (value, pl, time, apid) => { //троттлинг отправляет на сервер не раньше, чем через .5 сек после последнего изменения
        let xhr = this.xhr();
        xhr.open("POST", "https://betacab.gusli.net/api/v1/toggle/playlist", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onerror = function () {
            // debugger
            // alert('Ошибка ' + this.response);
        };
        xhr.onload = function () {
            if (this.response !== "{\"status\":\"ok\",\"affected\":true}") {
                // debugger
                // alert('Ошибка ' + this.response);
            }
        };
        xhr.send("place_id=" + apid + "&playlist_id=" + pl.id + "&toggle=on&dayPart=" + time + "&chance=" + value);
    };

    offPlaylist = (pl, time) => {
        let playListSettings = this.state.playListSettings;
        let apid = this.state.apid;
        if (playListSettings[apid][time].length > 1) {
            let t = this;
            let otherPlaylists = this.state.otherPlaylists;
            let playListsAll = this.state.playListsAll;
            let i = playListSettings[apid][time].indexOf(pl);
            let xhr = this.xhr();
            xhr.open("POST", "https://betacab.gusli.net/api/v1/toggle/playlist", true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xhr.onerror = function () {
                // debugger
                // alert('Ошибка ' + this.response);
            };
            xhr.onload = function () {
                if (this.response !== "{\"status\":\"ok\",\"affected\":true}") {
                    if (JSON.parse(this.responseText).status_code === 401) {
                        t.logout();
                    }
                    // debugger
                    // alert('Ошибка ' + this.response);
                } else {
                    pl.available = playListsAll.filter(function (item) {
                        return item.id === pl.id;
                    })[0].available;
                    otherPlaylists[apid][time].unshift(pl);
                    playListSettings[apid][time].splice(i, 1);
                    t.setState({"playListSettings": playListSettings, "otherPlaylists": otherPlaylists}, function () {
                        let playListSettings = t.state.playListSettings;
                        let playlistBank = t.state.playlistBank;
                        let id = t.state.activePlaylist;
                        t.mergePlacePlaylist(this, playlistBank, id, apid, playListSettings);
                    });
                }
            };
            xhr.send("place_id=" + apid + "&playlist_id=" + pl.id + "&toggle=off&dayPart=" + time);
        } else {
            //можно сказать пользователю, что нельзя удалять последний плейлист
        }
    };

    onPlaylist = (pl, time) => {
        let playListSettings = this.state.playListSettings;
        let apid = this.state.apid;
        let otherPlaylists = this.state.otherPlaylists;
        let i = otherPlaylists[apid][time].indexOf(pl);
        let t = this;
        let xhr = this.xhr();
        xhr.open("POST", "https://betacab.gusli.net/api/v1/toggle/playlist", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onerror = function () {
            if (JSON.parse(this.responseText).status_code === 401) {
                t.logout();
            }
            // debugger
            // alert('Ошибка ' + this.response);
        };
        xhr.onload = function () {
            if (this.response !== "{\"status\":\"ok\",\"affected\":true}") {
                if (JSON.parse(this.responseText).status_code === 401) {
                    t.logout();
                }
                // debugger
                // alert('Ошибка ' + this.response);
            } else {
                otherPlaylists[apid][time].splice(i, 1);
                pl.probability = pl.probability
                    ? pl.probability
                    : 100;
                playListSettings[apid][time].unshift(pl);
                t.setState({"playListSettings": playListSettings, "otherPlaylists": otherPlaylists}, function () {
                    let playListSettings = t.state.playListSettings;
                    let playlistBank = t.state.playlistBank;
                    let id = t.state.activePlaylist;
                    t.mergePlacePlaylist(t, playlistBank, id, apid, playListSettings);
                });
            }
        };
        xhr.send("place_id=" + apid + "&playlist_id=" + pl.id + "&toggle=on&dayPart=" + time + "&chance=100");
    };

    editPlaylist = (id) => {
        this.setState({"chapter": "EditPlaylist", "activePlaylist": id});
    };

    setPlace(id) {
        this.setState({"apid": id, "chapter": "PlaceSettings"});
    }

    menuClick(chapter) {
        if (chapter === "Money" || chapter === "Places") {
            this.askIfLog();
        }
        this.setState({chapter: chapter});
    }

    handleTouchTap = (event) => {
        event.preventDefault(); // This prevents ghost click.
        let userMenu = this.state.userMenu;
        userMenu.open = !userMenu.open;
        userMenu.anchorEl = event.currentTarget;
        this.setState({userMenu: userMenu});
    };

    profSettings = () => {
        this.setState({
            chapter: "Profile",
            userMenu: {
                open: false,
                anchorEl: false,
            },
        });
    };

    toggleFilter = (object, type, s) => {
        console.log("toggleFilter");
        let arr = this.state[type];
        let value = !object.on;
        let apid = this.state.apid;
        let t = this;
        object.on = value;
        let pointer = type.slice(0, -1);
        if (type === "songs") {
            pointer = "track";
        }
        let xhr = this.xhr();
        xhr.open("POST", "https://betacab.gusli.net/api/v1/toggle/order/" + pointer, true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onload = function () {
            let data = JSON.parse(this.response);
            if (!data.data) {
                if (JSON.parse(this.response).status_code === 401) {
                    t.logout();
                }
                // debugger
                // alert('Ошибка ' + this.response);
            } else {
                let i = arr[apid].indexOf(object);
                if (value && !s && type !== "genres") {
                    i = arr[apid].indexOf(object);
                    arr[apid].splice(i, 1);
                } else {
                    arr[apid].on = true;
                }
                let res = {};
                res[type] = arr;
                t.setState(res, function () {
                    if (!s && type !== "genres") {
                        data = data.data;
                        let res = {};
                        data.forEach(function (item, i) {
                            data[i].visible = true;
                        });
                        res[apid] = data;
                        let obj = {};
                        obj[type] = res;
                        t.setState(obj);
                    }
                });
            }
        };
        xhr.onerror = function () {
            // debugger
            // alert('Ошибка ' + this.response);
        };
        let body = "place_id=" + apid + "&" + pointer + "_id=" + object.id + "&toggle=" + (value
            ? "on"
            : "off");
        xhr.send(body);
    };

    toggleTrack = (object) => { // для popular
        let popular = this.state.popular;
        let apid = this.state.apid;
        let t = this;
        let xhr = this.xhr();
        xhr.open("POST", "https://betacab.gusli.net/api/v1/toggle/order/track", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onerror = function () {
            debugger;
            // alert('Ошибка ' + this.response);
        };
        xhr.onload = function () {
            let data = JSON.parse(this.response);
            if (!data.data) {
                if (JSON.parse(this.response).status_code === 401) {
                    t.logout();
                }
                // debugger
                // alert('Ошибка ' + this.response);
            } else {
                let i = popular[apid].indexOf(object);
                popular[apid].splice(i, 1);
                t.setState({"popular": popular});
            }
        };
        xhr.send("place_id=" + apid + "&track_id=" + object.id + "&toggle=on");
    };

    toggleStats = (object, time) => {
        let placeStats = this.state.placeStats;
        object.on = !object.on;
        let toggle = object.on
            ? "on"
            : "off";
        let t = this;
        let apid = this.state.apid;
        let xhr = this.xhr();
        xhr.open("POST", "https://betacab.gusli.net/api/v1/toggle/order/track", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onerror = function () {
            // debugger
            // alert('Ошибка ' + this.response);
        };
        xhr.onload = function () {
            let data = JSON.parse(this.response);
            if (!data.data) {
                if (JSON.parse(this.response).status_code === 401) {
                    t.logout();
                }
                // debugger
                // alert('Ошибка ' + this.response);
            } else {
                t.setState({"placeStats": placeStats});
            }
        };
        xhr.send("place_id=" + apid + "&track_id=" + object.id + "&toggle=" + toggle);
    };

    blockedToggle = (tid) => { //не забыть переброс
        let apid = this.state.apid;
        let blocked = this.state.blocked;
        let i = blocked[apid].indexOf(tid);
        let t = this;
        let toggle = "on";
        if (i === -1) {
            blocked[apid].push(tid);
            toggle = "off";
        } else {
            blocked[apid].splice(i, 1);
        }
        let xhr = this.xhr();
        xhr.open("POST", "https://betacab.gusli.net/api/v1/toggle/playlist/track", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onload = function () {
            if (this.response !== "{\"status\":\"ok\"}") {
                if (JSON.parse(this.responseText).status_code === 401) {
                    t.logout();
                }
                // debugger
                // alert('Ошибка ' + this.response);
            } else {
                t.setState({"blocked": blocked});
            }
        };
        xhr.onerror = function () {
            // debugger
            // alert('Ошибка ' + this.response);
        };
        let body = "place_id=" + apid + "&track_id=" + tid + "&toggle=" + toggle;
        xhr.send(body);
    };

    delTrack = (object) => { //не забыть переброс (пока не используется)
        let bank = this.state.playlistBank;
        let apl = this.state.activePlaylist;
        if (bank[apl].diy) {
            let i = bank[apl].tracks.indexOf(object);
            bank[apl].tracks.splice(i, 1);
        }
        this.setState({"playlistBank": bank});
    };

    logout = () => {
        this.setState(JSON.parse(initialState));
        let xhr = this.xhr();
        xhr.open("GET", "https://betacab.gusli.net/api/v1/auth/logout", true);
        xhr.onerror = function () {
            //alert('Ошибка ' + this.status);
        };
        xhr.send();
    };

    togglePermissions = (i) => {
        let apid = this.state.apid;
        let placeSettings = this.state.placeSettings;
        let arr = ["can_change_playlist", "can_play_local", "can_block_muslic"];
        placeSettings[apid][arr[i]] = !placeSettings[apid][arr[i]];
        let t = this;
        let xhr = this.xhr();
        xhr.open("POST", "https://betacab.gusli.net/api/v1/place/" + apid + "/settings/playerPermissions", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onerror = function () {
            debugger;
            // alert('Ошибка ' + this.response);
        };
        xhr.onload = function () {
            let data = JSON.parse(this.response);
            if (!data.data) {
                if (JSON.parse(this.response).status_code === 401) {
                    t.logout();
                }
                // debugger
                // alert('Ошибка ' + this.response);
            } else {
                placeSettings[apid][arr[i]] = !placeSettings[apid][arr[i]];
                t.setState({"placeSettings": placeSettings});
            }
        };
        xhr.send("can_block_muslic=" + placeSettings[apid].can_block_muslic + "&can_change_playlist=" + placeSettings[apid].can_change_playlist + "&can_play_local=" + placeSettings[apid].can_play_local);
    };

    loadPlayer = () => {
        let os = window.navigator.platform;
        if (os.match("Android")) {
            return 'android'
        } else if (os.match('Win')) {
            return 'windows'
        } else { // dev
            return 'android'
        }
    }

    closeDialog = () => {
        this.setState({'loadDialog': false});
    }

    openDialog = () => {
        this.setState({'loadDialog': true});
    }

    setNum = () => {
      this.setState({'num': 0});
    }

    renewOwner = (data) => {
      data.name=data.owner;
      this.setState({'owner':data});
    }

    render() {
        const t = this;
        // console.log(this.state.login);
        // if (this.state.login) {
        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div>
                    <Dialog title="Выберете вашу OS" modal={false} open={this.state.loadDialog}
                            className="downloadDialog">
                        <div className="col">

                            {this.loadPlayer() === 'android' ?
                                <div>
                                    <a download href="https://betacab.gusli.net/api/v1/player/180/android"
                                       onClick={() => this.closeDialog()}>
                                        <RaisedButton
                                            primary={true}
                                            label="Android"
                                            icon={<FontIcon className="fa fa-android"/>}
                                        /></a>
                                    <a download href="https://betacab.gusli.net/api/v1/player/180/windows"
                                       onClick={() => this.closeDialog()}>
                                        <FlatButton
                                            label="Windows"
                                            icon={<FontIcon className="fa fa-windows"/>}
                                        /></a>
                                    <br/>
                                    <FlatButton
                                        label="Отмена"
                                        onClick={() => this.closeDialog()}
                                    /></div> :
                                <div><a download onClick={() => this.closeDialog()}
                                        href="https://betacab.gusli.net/api/v1/player/180/windows"><RaisedButton
                                    primary={true}
                                    label="Windows"
                                    icon={<FontIcon className="fa fa-windows"/>}
                                /></a>
                                    <a download href="https://betacab.gusli.net/api/v1/player/180/android"
                                       onClick={() => this.closeDialog()}><FlatButton
                                        label="Android"
                                        icon={<FontIcon className="fa fa-android"/>}
                                    /></a>
                                    <br/>
                                    <FlatButton
                                        label="Отмена"
                                        onClick={() => this.closeDialog()}
                                    />
                                </div>
                            }
                        </div>
                    </Dialog>

                    <Toolbar className="appHeader">
                        <ToolbarGroup>
                            <RaisedButton label="Скачать плеер" primary={true} onClick={() => this.openDialog()}/>
                        </ToolbarGroup>
                        <ToolbarGroup>
                            <p>Баланс: {this.state.num / 100} {this.state.currency}<br/>
                                <div className="clickText" onClick={() => this.menuClick("Money")}>Вывести
                                    средства
                                </div>
                            </p>
                        </ToolbarGroup>
                        <ToolbarGroup>
                            <div className="ownerName">{this.state.owner.name}</div>
                            <IconButton onClick={this.handleTouchTap}>
                                <FontIcon className="fa fa-chevron-down"/>
                            </IconButton>
                            <Popover className="bigJeam" open={this.state.userMenu.open}
                                     anchorEl={this.state.userMenu.anchorEl} anchorOrigin={{
                                horizontal: "left",
                                vertical: "bottom",
                            }} targetOrigin={{
                                horizontal: "left",
                                vertical: "top",
                            }} useLayerForClickAway={false}>
                                <Menu>
                                    <MenuItem primaryText="Редактировать профиль"
                                              onClick={() => this.profSettings()}/>
                                    <MenuItem primaryText="Выйти" onClick={() => this.logout()}/>
                                </Menu>
                            </Popover>
                        </ToolbarGroup>
                    </Toolbar>
                    <div className="leftColumn">
                        <List className="menu">
                            <ListItem primaryText="Статистика" onClick={() => this.menuClick("Stats")}/>
                            <ListItem primaryText="Музыка" onClick={() => this.menuClick("Music")}/>
                            <ListItem primaryText="Фильтр музыки" onClick={() => this.menuClick("Filter")}/>
                            <ListItem primaryText="Топ популярных" onClick={() => this.menuClick("Popular")}/>
                            <ListItem primaryText="Мои заведения" onClick={() => this.menuClick("Places")}/>
                            <ListItem primaryText="Помощь" onClick={() => this.menuClick("Help")}/>
                        </List>
                        <div className="supInfo">
                            Тех. поддержка:<br/>
                            8(800)-234-12-64<br/>
                            8(905)-224-96-96<br/>
                            <br/>
                            support@gusli.net
                        </div>
                    </div>
                    <div className="content">
                        {(() => {
                            switch (this.state.chapter) {

                                case "Music":
                                    return <Music
                                        apid={this.state.apid}
                                        changeApid={(apid) => this.changeApid(apid)}
                                        editPlaylist={(id) => this.editPlaylist(id)}
                                        playListSettings={this.state.playListSettings}
                                        genres={this.state.playlistGenres} //не путать с жанрами для фильтра
                                        otherPlaylists={this.state.otherPlaylists}
                                        places={this.state.places}
                                        getPlayListSettings={(id) => this.getPlayListSettings(id)}
                                        offPlaylist={(pl, time) => this.offPlaylist(pl, time)}
                                        onPlaylist={(pl, time) => this.onPlaylist(pl, time)}
                                        setProb={(value, pl, time) => this.setProb(value, pl, time)}/>;

                                case "Filter":
                                    return <Filter
                                        apid={this.state.apid}
                                        changeApid={(apid) => this.changeApid(apid)}
                                        genres={this.state.genres}
                                        artists={this.state.artists}
                                        songs={this.state.songs}
                                        places={this.state.places}
                                        getSongs={() => this.getFilterSongs()}
                                        getArtists={() => this.getFilterArtists()}
                                        getGenres={() => this.getFilterGenres()}
                                        toggle={(object, type, s) => this.toggleFilter(object, type, s)}
                                        durationFormater={(duration) => this.durationFormater(duration)}/>;

                                case "Popular":
                                    return <Popular
                                        apid={this.state.apid}
                                        changeApid={(apid) => this.changeApid(apid)}
                                        places={this.state.places} popular={this.state.popular}
                                        getPopular={() => this.getPopular()}
                                        toggle={(object) => this.toggleTrack(object)}/>;

                                case "Places":
                                    return <Places
                                        apid={this.state.apid}
                                        places={this.state.places}
                                        setPlace={(id) => this.setPlace(id)}/>;

                                case "PlaceSettings":
                                    return <PlaceSettings
                                        apid={this.state.apid}
                                        changeApid={(apid) => this.changeApid(apid)}
                                        places={this.state.places}
                                        getPlaceSettings={() => this.getPlaceSettings()}
                                        placeSettings={this.state.placeSettings}
                                        togglePermissions={(i) => this.togglePermissions(i)}/>;

                                case "EditPlaylist":
                                    return <EditPlaylist
                                        id={this.state.activePlaylist}
                                        getPlaylist={() => this.getPlaylist()}
                                        apid={this.state.apid}
                                        changeApid={(apid) => this.changeApid(apid)}
                                        places={this.state.places} back={() => {
                                        t.setState({"chapter": "Music"});
                                    }} playlistBank={this.state.playlistBank}
                                        blocked={this.state.blocked}
                                        toggle={(tid) => this.blockedToggle(tid)}
                                        delTrack={(object) => this.delTrack(object)}/>;

                                case "Profile":
                                    return <Profile owner={this.state.owner} renewOwner={(data) => this.renewOwner(data)}/>;

                                case "Money":
                                    return <Money
                                        refLink={this.state.refLink}
                                        refs={this.state.owner.refs}
                                        places={this.state.places}
                                        payouts={this.state.payouts}
                                        payoutMethod={this.state.payoutMethod}
                                        currency={this.state.currency}
                                        total={this.state.balance}
                                        getPayouts={() => this.getPayouts()}
                                        setNum={() => this.setNum()}
                                        num={this.state.num}/>; //для понятности переименовать в balance

                                case "Help":
                                    return <Help/>;

                                case "Stats":
                                    return <Stats
                                        apid={this.state.apid}
                                        changeApid={(apid) => this.changeApid(apid)}
                                        getPlaceStats={() => this.getPlaceStats()}
                                        placeStats={this.state.placeStats}
                                        places={this.state.places}
                                        toggle={(object, time) => this.toggleStats(object, time)}/>;

                                default:
                                    return "";
                            }
                        })()}
                    </div>
                </div>

            </MuiThemeProvider>
        );
        // }
    }

    async tryLoginByToken() {
        let t = this;
        let request = async function (method, url, async, data, onload) {
            let XHR = ("onload" in new XMLHttpRequest())
                ? XMLHttpRequest
                : XDomainRequest;
            let xhr = new XHR();

            xhr.withCredentials = true;
            xhr.open(method, url, async);

            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xhr.setRequestHeader("charset", "UTF-8");

            xhr.onload = onload;
            await xhr.send(QueryString.stringify(data));
        };

        let queryArr = QueryString.parse(window.location.search);

        function onload() {
            let data = JSON.parse(this.response);
            if (data && !data.error) {
                data = data.data;
                data.name = data.owner;
                delete(data.owner);
                t.setState({"owner": data, "currency": data.currency, "num": data.balance, "login": true});
                t.getPlaces();
                t.getPlayListsAll();
                window.location.replace('/')
            }
        }

        if (queryArr && queryArr["laravel_session"]) {
            await request("POST", "https://betacab.gusli.net/api/v1/auth/login/token", true, queryArr, onload);
        }

    }
}

export default App;
