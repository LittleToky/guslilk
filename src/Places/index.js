import React, {Component} from 'react';

// Material-UI
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';

// Font
import 'typeface-roboto'

class Places extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      apid: props.apid,
      places: props.places
    }
  }

  toggleTrack = (index) => { // проверить
    let tracks = this.state.tracks;
    tracks[index].on = !tracks[index].on;
    this.setState({tracks: tracks});
  }

  componentWillReceiveProps(props) {
    this.setState({'places': props.places, 'apid': props.apid});
  }

  render() {
    //const state = this.state;
    return (
      <div >
        <p className="pageHeader">Заведения</p>
        <Table selectable={false} className="placeTable">
          <TableHeader>
            <TableRow>
              <TableHeaderColumn className="places">Заведение</TableHeaderColumn>
              <TableHeaderColumn className="places">Управляющий</TableHeaderColumn>
              <TableHeaderColumn className="places">Плеер для заведения</TableHeaderColumn>
              <TableHeaderColumn className="places">Действия</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody>
            {this.state.places.map((row, index) => (
              <TableRow key={index} className={row.id===this.props.apid?"places active":"places"}>
                <TableRowColumn>
                  <div className="col">
                    <img src={row.imgUrl} className="plaseImage"/>
                  </div>
                  <div className="col">
                    <div className="placeTitle" onClick={() => this.props.setPlace(row.id)}>{row.title}</div>
                    <div className="subText">{row.address}</div>
                  </div>
                </TableRowColumn>
                <TableRowColumn>{row.manager}</TableRowColumn>
                <TableRowColumn>
                  <RaisedButton label="Скачать плеер"/>
                </TableRowColumn>
                <TableRowColumn>
                  <IconButton className="smallIc">
                    <FontIcon className="fa fa-cogs" onClick={() => this.props.setPlace(row.id)}/>
                  </IconButton>
                  {/*<IconButton className="smallIc">
                    <FontIcon className="fa fa-times"/>
                  </IconButton>*/}
                </TableRowColumn>
              </TableRow>
            ))}
          </TableBody>
        </Table><br/>
        {/*<RaisedButton label="Добавить"/>*/}
      </div>
    );
  }
}

export default Places;
