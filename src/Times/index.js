import React, {Component} from 'react';

// Material-UI
// Font
import 'typeface-roboto'

const st = {
  wr: {
    'height': '150px',
    'width': '150px',
    'display': 'block',
    'overflow': 'hidden',
    'borderRadius': '50%',
    'position': 'absolute'
  },
  st0: {
    'fill': '#3a3e56'
  },
  st1: {
    'fill': '#56eab4'
  },
  st2: {
    'fill': '#fae57f'
  },
  st3: {
    'fill': '#EA565A'
  },
  stc: {
    'fill': '#dadef0',
    'stroke' : '#999cae'
  }
}

class Times extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      times: this.props.times
    }
  }

  componentDidMount() {
    var that = this;
    document.querySelectorAll('.timeLabels div').forEach(function(item, i) {
      item.onmousedown = function(e) {
        that.startChange(that, e, i)
      };
    });
  }

  componentWillReceiveProps(props) {
    this.setState({'times': props.times});
  }

  startChange(t, e, i) {
    var that = this;
    t.setState({pointer: i});
    var timeElem = document.querySelectorAll('.timeLabels')[0];
    document.querySelectorAll('.timeLabels div')[i].className='active';
    timeElem.onmousemove = function(e) {
      let x = e.pageX - 365;
      let y = 404 - e.pageY;
      let time;
      let m = 13700;
      if (y >= 0 && x >= 0) {
        time = (m) * Math.atan(x / y);
      }
      if (y >= 0 && x < 0) {
        time = m * Math.atan(x / y);
        time = time + 86400;
      }
      if (y < 0 && x >= 0) {
        time = -m * (Math.PI - Math.atan(x / y));
        time = time + 86400;
      }
      if (y < 0 && x <= 0) {
        time = -m * (Math.atan(x / y));
        time = 86400 / 2 - time;
      }
      time = Math.round(time / 300) * 300;
      let t = JSON.parse(JSON.stringify(that.state.times));
      t[i] = time;
      let cond=(t[0]<=t[1] && t[1]<=t[2] && t[2]<=t[3]) || (t[3]<=t[0] && t[0]<=t[1] && t[1]<=t[2]) || (t[2]<=t[3] && t[3]<=t[0] && t[0]<=t[1]) || (t[1]<=t[2] && t[2]<=t[3] && t[3]<=t[0]);
      if (cond) {
        that.props.change(t);
      }
    }
    timeElem.onmouseup = function() {
      timeElem.onmouseup = undefined;
      timeElem.onmousemove = undefined;
      document.querySelectorAll('.timeLabels div.active')[0].className='';
    }
    let timesElem=document.getElementById('Times');
    timesElem.onmouseleave = function() {
      timesElem.onmouseleave = undefined;
      timeElem.onmousemove = undefined;
      let point=document.querySelectorAll('.timeLabels div.active')[i];
      if (point) {
        point.className='';
      }
    }
  }

  getTimes = () => {
    let times = this.state.times;
    let res = [];
    times.forEach(function(item) {
      item = item / 60;
      let hours = Math.floor(item / 60) + '';
      let minutes = item % 60 + '';
      hours = (hours.length === 1)
        ? ('0' + hours)
        : hours;
      minutes = (minutes.length === 1)
        ? ('0' + minutes)
        : minutes;
      res.push(hours + ':' + minutes);
    });
    return res
  }

  vx = (t) => {
    return (Math.sin(Math.PI * t / 720) * 120 + 75).toFixed(2)
  }

  vy = (t) => {
    return (-Math.cos(Math.PI * t / 720) * 120 + 75).toFixed(2)
  }

  poly = (t1, t2) => {
    let tm1 = (t2 - t1) / 4;
    tm1 = (tm1 > 0)
      ? tm1
      : (360 + tm1);
    let tm2 = t1 + 2 * tm1;
    let tm3 = tm2 + tm1;
    tm1 = t1 + tm1;

    let x1 = this.vx(t1);
    let y1 = this.vy(t1);

    let xm1 = this.vx(tm1);
    let ym1 = this.vy(tm1);

    let xm2 = this.vx(tm2);
    let ym2 = this.vy(tm2);

    let xm3 = this.vx(tm3);
    let ym3 = this.vy(tm3);

    let x2 = this.vx(t2);
    let y2 = this.vy(t2);

    return "75,75 " + x1 + ',' + y1 + " " + xm1 + "," + ym1 + " " + xm2 + ',' + ym2 + " " + xm3 + ',' + ym3 + " " + x2 + ',' + y2;
  }

  label = (t) => {
    let x = (Math.sin(Math.PI * t / 720) * 75 + 75).toFixed(0);
    let y = (-Math.cos(Math.PI * t / 720) * 75 + 75).toFixed(0);
    return {
      'top': y + 'px',
      'left': x + 'px'
    }
  }

  labelTurn = (t) => {
    let x = (Math.sin(Math.PI * t / 720) * 30 - 15).toFixed(0);
    let y = (-Math.cos(Math.PI * t / 720) * 15 - 20).toFixed(0);
    return {
      'top': y + 'px',
      'left': x + 'px'
    }
  }

  render() {
    let times = this.getTimes();
    let pt = this.state.times;
    let polys = [
      this.poly(pt[0] / 60, pt[1] / 60),
      this.poly(pt[1] / 60, pt[2] / 60),
      this.poly(pt[2] / 60, pt[3] / 60)
    ];

    let labels = [
      this.label(pt[0] / 60),
      this.label(pt[1] / 60),
      this.label(pt[2] / 60),
      this.label(pt[3] / 60)
    ]

    let turn = [
      this.labelTurn(pt[0] / 60),
      this.labelTurn(pt[1] / 60),
      this.labelTurn(pt[2] / 60),
      this.labelTurn(pt[3] / 60)
    ]

    return (
      <div className="Times" id="Times">
        <div style={st.wr}>
          <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px">
            <circle style={st.st0} cx="75" cy="75" r="75"/>
            <polygon style={st.st1} points={polys[0]}/>
            <polygon style={st.st2} points={polys[1]}/>
            <polygon style={st.st3} points={polys[2]}/>
            <circle style={st.stc} cx="75" cy="75" r="40"/>
          </svg>
        </div>
        <div className="timeLabels">
          <div style={labels[0]}>
            <p style={turn[0]}>{times[0]}</p>
          </div>
          <div style={labels[1]}>
            <p style={turn[1]}>{times[1]}</p>
          </div>
          <div style={labels[2]}>
            <p style={turn[2]}>{times[2]}</p>
          </div>
          <div style={labels[3]}>
            <p style={turn[3]}>{times[3]}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Times;
