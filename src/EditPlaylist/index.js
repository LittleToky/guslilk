import React, {Component} from 'react';

// Material-UI
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import {List, ListItem} from 'material-ui/List';
//import {Toolbar} from 'material-ui/Toolbar';
import FlatButton from 'material-ui/FlatButton';
//import MenuItem from 'material-ui/MenuItem';
//import SelectField from 'material-ui/SelectField';
import RefreshIndicator from 'material-ui/RefreshIndicator';

// Font
import 'typeface-roboto'

const style = {
  container: {
    position: 'relative',
  },
  refresh: {
    display: 'inline-block',
    position: 'relative',
  },
};

class EditPlaylist extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      selected: [],
      id: props.id, //id плейлиста
      places: props.places,
      apid: props.apid,
      playlistBank: props.playlistBank,
      blocked: props.blocked,
      audio: '',
      paused: false
    }
  }

  componentDidMount() {
    this.props.getPlaylist();
  }

  componentWillReceiveProps(props) {
    this.setState({'blocked': props.blocked, 'id': props.id, 'places': props.places, 'apid': props.apid, 'playlistBank': props.playlistBank});
  }

  play = (url) => {
    let audio=document.getElementById('filterAudio');
    if (url!==audio.src) {
      audio.pause();
      this.setState({'audio':url,'paused':true},function(){
        audio.src=this.state.audio;
        audio.play();
        this.setState({'audio':url,'paused':false});
      });
    } else {
      this.setState({'paused':false});
      audio.play();
    }
  }

  pause = () => {
    let audio=document.getElementById('filterAudio');
    this.setState({'paused':true});
    audio.pause();
  }

  audioEnded = () => {
    this.setState({paused: true});
  }

  render() {
    let apid=this.state.apid;
    let pl=this.state.playlistBank[this.state.id];
    let blocked=this.state.blocked[apid];
    let on=(pl&&pl.selected&&pl.selected[apid])?pl.selected[apid].join(', '):'';
    let audio=document.getElementById('filterAudio');
    let src=this.state.audio;
    let paused=audio?audio.paused:true;
    return (
      !!(apid && pl && blocked)?<div className="editPL">
        {/*<Toolbar className="toolbar">
          <SelectField value={apid} onChange={(event, index, id) => this.props.changeApid(id)}>
            {this.state.places.map((row, index) => (<MenuItem key={index} value={row.id} primaryText={row.title}/>))}
          </SelectField>
          <FlatButton label="Применить ко всем" className="toolbarButton"/>
        </Toolbar>*/}
        <FlatButton
          label="Назад"
          icon={<FontIcon className="fa fa-chevron-left" onClick={() => this.props.back()}/>}
        />
        <br/>
        <div className="col">
          <img className="albumArt" src={pl?pl.url:''}/>
        </div>
        <div className="col">
          <b>{(pl&&pl.title)?pl.title:''}</b><br/>
          <br/>
          <div className="medText">Длительность: {(pl&&pl.duration)?pl.duration:''}<br/>
            Всего треков: {(pl&&pl.num)?pl.num:''}<br/>
            <br/>
            Выбрано: {on}</div>
        </div>
        <List>
          {pl&&pl.tracks?pl.tracks.map((row, index) => (
            <ListItem className="pops sb" key={index}>
              <div className="col"></div>
              <div className="col">
                <IconButton>
                {(paused && (audio && audio.src!=='')) || row.url!==src? //
                  <FontIcon className="fa fa-play-circle" onClick={() => this.play(row.url)}/>:
                  <FontIcon className="fa fa-pause-circle" onClick={() => this.pause()}/>
                }
                </IconButton>
              </div>
              <div className="col">
                {row.title}
                <div className="subText">{row.artist}</div>
              </div>
              <div className="col right">
                <IconButton >
                  <FontIcon className={(blocked&&blocked.indexOf(row.tid)===-1)
                    ? "fa fa-volume-up green"
                    : "fa fa-volume-off red"} onClick={() => this.props.toggle(row.tid)}/>
                </IconButton>
                <IconButton className={pl&&pl.diy
                  ? ""
                  : "hidden"}>
                  <FontIcon className="fa fa-close" onClick={() => this.props.delTrack(row)}/>
                </IconButton>
              </div>
              <div className="col right duration">
                {row.duration}
              </div>
            </ListItem>
          )):''}
        </List><audio id="filterAudio" onEnded={() => this.audioEnded()}></audio>
      </div>:<RefreshIndicator
      size={50}
      left={70}
      top={0}
      loadingColor="#FF9800"
      status="loading"
      style={style.refresh}
    />
    );
  }
}

export default EditPlaylist;
