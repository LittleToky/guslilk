import React, {Component} from "react";

// Material-UI

//import FontIcon from "material-ui/FontIcon";
import TextField from "material-ui/TextField";
import RaisedButton from "material-ui/RaisedButton";

// Font
import "typeface-roboto";

class Registration extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            text: "1111",
            email: "Taurus85",
            phone: "77777777777",
        };
    }

    change = (v, key) => {
        let obj = {};
        obj[key] = v;
        this.setState(obj);
    };

    pwdVisTog = () => {
        this.setState({"visible": !this.state.visible});
    };

    render() {
        return (
            <div className="centred-auth-block">
                <div className="Registration panel">
                    <TextField hintText="Телефон" value={this.state.phone}
                               onChange={(e, v) => this.change(v, "phone")}/><br/>
                    <TextField hintText="Email" value={this.state.email}
                               onChange={(e, v) => this.change(v, "email")}/><br/>

                    <RaisedButton label="Зарегистрироваться" primary={true} onClick={() => this.props.registration("77777777777", "Taurus85")}/>
                </div>
            </div>
        );
    }
}

export default Registration;
