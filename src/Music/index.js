import React, {Component} from 'react';

// Material-UI
import {Tabs, Tab} from 'material-ui/Tabs';
import Paper from 'material-ui/Paper';
import {List, ListItem} from 'material-ui/List';
import TextField from 'material-ui/TextField';
//import FlatButton from 'material-ui/FlatButton';
import Slider from 'material-ui/Slider';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
//import Dialog from 'material-ui/Dialog';
//import MenuItem from 'material-ui/MenuItem';
//import SelectField from 'material-ui/SelectField';
//import {Toolbar} from 'material-ui/Toolbar';
import RefreshIndicator from 'material-ui/RefreshIndicator';

// Font
import 'typeface-roboto'

const styles = {
  tbGroup: {
    width: '100%'
  },
  aside: {
    width: 'auto',
    marginLeft: '5%'
  }
}

const style = {
  container: {
    position: 'relative',
  },
  refresh: {
    display: 'inline-block',
    position: 'relative',
  },
};

class Music extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      text: '',
      genres: props.genres,
      chapter: props.chapter,
      addDialogOpen: false,
      newPlayListName: '',
      playListsAll: props.playListsAll,
      otherPlaylists: props.otherPlaylists,
      playListSettings: props.playListSettings,
      places: props.places,
      apid: props.apid,
      asked: props.asked
    }
  }

  componentDidMount() {
    this.props.getPlayListSettings(); //на самом-то деле не здесьдолжно быть, а первый раз, когда устаканились апид и сетинги
  }

  componentDidUpdate() {
    //console.log('componentDidUpdate Music')
  }

  componentWillReceiveProps(props) {
    let apid=this.state.apid;
    let asked=this.state.asked;

    if (apid && !asked) {
      this.props.getPlayListSettings();
      this.setState({'asked':true});
    }

    this.setState({
      'playListSettings': props.playListSettings,
      'apid': props.apid,
      'places': props.places,
      'otherPlaylists': props.otherPlaylists
    });
  }

  closeDialog = () => {
    this.setState({'addDialogOpen': false});
  }

  handleGenreClick = (value) => {
    let val = this.state.genres[value].title;
    this.setState({text: val})
    this.handleUpdateGenreInput(val);
    //this.handleUpdateGenreInput(val, true);
  }

  handleUpdateGenreInput = (v) => {
    v = v.toLowerCase()
    let apid = this.state.apid;
    let other = this.state.otherPlaylists;
    let playListSettings = this.state.playListSettings;
    let genres = this.state.genres;
    genres.forEach(function(item, i) {
      genres[i].visible = (genres[i].title.toLowerCase().indexOf(v) === -1)
        ? false
        : true;
    });
    for (let key in other[apid]) { // просто чтоб были ключи времени дня
      other[apid][key].forEach(function(pl, p) {
        if (pl.available) {
          other[apid][key][p].visible = pl.genres.filter(function(gen) {
            return gen.toLowerCase().indexOf(v) !== -1;
          })[0]
            ? true
            : false;
        }
      });
      playListSettings[apid][key].forEach(function(pl, p) {
        playListSettings[apid][key][p].visible = pl.genres.filter(function(gen) {
          return gen.toLowerCase().indexOf(v) !== -1;
        })[0]
          ? true
          : false;
      });
    }
    this.setState({playListSettings: playListSettings, otherPlaylists: other, genres: genres, text: v.toLowerCase()});
  }

  openDialog = (e, n) => {
    this.setState({addDialogOpen: true})
  }

  newPlayListName = (e, n) => {
    this.setState({newPlayListName: n});
  }

  chooseFile = (e) => {
    e.target.closest('.col').querySelector('input.hidden').click();
  }

  addFile = (e) => {
    //let file = e.target.files[0];
    //let reader = new FileReader();
    //debugger
    // reader.onload = function(e) {
    //   let dv = new DataView(this.result);
    //   debugger
    //   if (dv.getString(3, dv.byteLength - 128) === 'TAG') {
    //     let title = dv.getString(30, dv.tell());
    //     let artist = dv.getString(30, dv.tell());
    //     let album = dv.getString(30, dv.tell());
    //     let year = dv.getString(4, dv.tell());
    //     console.log(title + " - " + artist)
    //   } else {
    //     // no ID3v1 data found.
    //   }
    // };
    //
    // reader.readAsArrayBuffer(file);
    //this.state.audio.src=URL.createObjectURL(file);
  }

  render() {
    let apid = this.state.apid;
    let settings = this.state.playListSettings[apid];
    let other = this.state.otherPlaylists[apid];
    return (
      !!(apid && settings && other)?<div>
        {/*<Dialog title="Загрузите свой плелист" actions={[ < FlatButton label = "Отмена" primary = {
            false
          }
          onClick = {
            () => this.closeDialog()
          } />, < FlatButton label = "Сохранить" primary = {
            false
          }
          keyboardFocused = {
            true
          }
          onClick = {
            this.handleClose
          } />
        ]} modal={false} open={this.state.addDialogOpen} className="addPlaylistDialog">
          <div className="col">
            <Paper zDepth={1} className="ghostPaper" onClick={() => this.openDialog()}>
              <IconButton>
                <FontIcon className={"fa fa-plus"}/>
              </IconButton><br/>
              Добавить<br/>
              обложку
            </Paper>
          </div>
          <div className="col">
            <TextField fullWidth={true} hintText="Название плейлиста" onChange={() => this.newPlayListName()} defaultValue={this.state.newPlayListName}/><br/>
            <FlatButton label="Выбрать файлы" primary={false} fullWidth={true} onClick={() => this.chooseFile}/>
            <input type="file" accept="audio/*" className="hidden" onChange={() => this.addFile()}/>
          </div>
        </Dialog>*/}
        {/*<Toolbar className="toolbar">
          <SelectField value={this.state.apid} onChange={(event, index, id) => this.props.changeApid(id)}>
            {this.state.places.map((row, index) => (<MenuItem key={index} value={row.id} primaryText={row.title}/>))}
          </SelectField>
          <FlatButton label="Применить ко всем" className="toolbarButton"/>
        </Toolbar>*/}
        <Tabs className="timeTabs col">
          <Tab label="Утро" value="a">
            <div className="col">
              {settings
                ? settings.morning.map((row, index) => (
                  <Paper zDepth={1} className={'Paper' + (row.visible
                    ? ''
                    : ' hidden')} key={index}>
                    <div className="paperTop">
                      <div className="playlistTitle" onClick={() => this.props.editPlaylist(row.id)}>{row.title}</div>
                      <IconButton className="paperButton" onClick={() => this.props.offPlaylist(row, 'morning')}>
                        <FontIcon className={"fa fa-volume-up green"}/>
                      </IconButton>
                    </div><br/>
                    <Slider className="playListsSlider" defaultValue={row.probability} min={0} max={100} step={1} onChange={(e, value) => this.props.setProb(value, row, 'morning')}/>
                    <div className="col prob">
                      {row.probability + '%'}
                    </div>
                  </Paper>
                ))
                : ''}
            </div>
            <div className="col">
              {other
                ? other.morning.map((row, index) => (
                  <Paper zDepth={1} className={'Paper' + ((row.visible && row.available)
                    ? ''
                    : ' hidden')} key={index}>
                    <div className="paperTop">
                      <div className="playlistTitle" onClick={() => this.props.editPlaylist(row.id)}>{row.title}</div>
                      <IconButton className="paperButton" onClick={() => this.props.onPlaylist(row, 'morning')}>
                        <FontIcon className={"fa fa-volume-off red"}/>
                      </IconButton>
                    </div>
                  </Paper>
                ))
                : ''}
              {/*}<Paper zDepth={1} className="ghostPaper" onClick={() => this.openDialog()}>
                <IconButton>
                  <FontIcon className={"fa fa-plus"}/>
                </IconButton><br/>
                Добавить<br/>
                <nobr>свой плейлист</nobr>
              </Paper>*/}
            </div>
          </Tab>
          <Tab label="День" value="b">
            <div className="col">
              {settings
                ? settings.day.map((row, index) => (
                  <Paper zDepth={1} className={'Paper' + (row.visible
                    ? ''
                    : ' hidden')} key={index}>
                    <div className="paperTop">
                      <div className="playlistTitle" onClick={() => this.props.editPlaylist(row.id)}>{row.title}</div>
                      <IconButton className="paperButton" onClick={() => this.props.offPlaylist(row, 'day')}>
                        <FontIcon className={"fa fa-volume-up green"}/>
                      </IconButton>
                    </div><br/>
                    <Slider className="playListsSlider" defaultValue={row.probability} min={0} max={100} step={1} onChange={(e, value) => this.props.setProb(value, row, 'day')}/>
                    <div className="col prob">
                      {row.probability + '%'}
                    </div>
                  </Paper>
                ))
                : ''}
            </div>
            <div className="col">
              {other
                ? other.day.map((row, index) => (
                  <Paper zDepth={1} className={'Paper' + ((row.visible && row.available)
                    ? ''
                    : ' hidden')} key={index}>
                    <div className="paperTop">
                      <div className="playlistTitle" onClick={() => this.props.editPlaylist(row.id)}>{row.title}</div>
                      <IconButton className="paperButton" onClick={() => this.props.onPlaylist(row, 'day')}>
                        <FontIcon className={"fa fa-volume-off red"}/>
                      </IconButton>
                    </div>
                  </Paper>
                ))
                : ''}
              {/*<Paper zDepth={1} className="ghostPaper" onClick={() => this.openDialog()}>
                <IconButton>
                  <FontIcon className={"fa fa-plus"}/>
                </IconButton><br/>
                Добавить<br/>
                <nobr>свой плейлист</nobr>
              </Paper>*/}
            </div>
          </Tab>
          <Tab label="Вечер" value="c">
            <div className="col">
              {settings
                ? settings.evening.map((row, index) => (
                  <Paper zDepth={1} className={'Paper' + (row.visible
                    ? ''
                    : ' hidden')} key={index}>
                    <div className="paperTop">
                      <div className="playlistTitle" onClick={() => this.props.editPlaylist(row.id)}>{row.title}</div>
                      <IconButton className="paperButton" onClick={() => this.props.offPlaylist(row, 'evening')}>
                        <FontIcon className={"fa fa-volume-up green"}/>
                      </IconButton>
                    </div><br/>
                    <Slider className="playListsSlider" defaultValue={row.probability} min={0} max={100} step={1} onChange={(e, value) => this.props.setProb(value, row, 'evening')}/>
                    <div className="col prob">
                      {row.probability + '%'}
                    </div>
                  </Paper>
                ))
                : ''}
            </div>
            <div className="col">
              {other
                ? other.evening.map((row, index) => (
                  <Paper zDepth={1} className={'Paper' + ((row.visible && row.available)
                    ? ''
                    : ' hidden')} key={index}>
                    <div className="paperTop">
                      <div className="playlistTitle" onClick={() => this.props.editPlaylist(row.id)}>{row.title}</div>
                      <IconButton className="paperButton" onClick={() => this.props.onPlaylist(row, 'evening')}>
                        <FontIcon className={"fa fa-volume-off red"}/>
                      </IconButton>
                    </div>
                  </Paper>
                ))
                : ''}
              {/*<Paper zDepth={1} className="ghostPaper" onClick={() => this.openDialog()}>
                <IconButton>
                  <FontIcon className={"fa fa-plus"}/>
                </IconButton><br/>
                Добавить<br/>
                <nobr>свой плейлист</nobr>
              </Paper>*/}
            </div>
          </Tab>
        </Tabs>
        <div className="col" style={styles.aside}>
          <TextField className="genreInput" hintText="Все" value={this.state.text} onChange={(e, v) => this.handleUpdateGenreInput(v)}/>
          <IconButton onClick={() => this.handleUpdateGenreInput('')} className={this.state.text===""?'hidden':''}>
            <FontIcon className={"fa fa-close"}/>
          </IconButton>
          <List>
            {this.state.genres.map((row, index) => (
              <ListItem className={(row.visible)
                ? 'genreListItem'
                : 'genreListItem hidden'} key={index} onClick={() => this.handleGenreClick(index)}>
                <div>{row.title}</div>
              </ListItem>
            ))}
          </List>
        </div>
      </div>:<RefreshIndicator
      size={50}
      left={70}
      top={0}
      loadingColor="#FF9800"
      status="loading"
      style={style.refresh}
    />
    );
  }
}

export default Music;
