import React, {Component} from "react";

// Material-UI

import FontIcon from "material-ui/FontIcon";
import TextField from "material-ui/TextField";
import RaisedButton from "material-ui/RaisedButton";

// Font
import "typeface-roboto";

class Login extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            text: "1111",
            pwd: "Taurus85",
            phone: "77777777777",
            visible: false,
        };
    }

    change = (v, key) => {
        let obj = {};
        obj[key] = v;
        this.setState(obj);
    };

    pwdVisTog = () => {
        this.setState({"visible": !this.state.visible});
    };

    render() {
        return (
            <div className="centred-auth-block">
                <div className="Login panel">
                    <TextField hintText="Логин/Телефон" value={this.state.phone}
                               onChange={(e, v) => this.change(v, "phone")}/><br/>
                    <TextField hintText="Пароль" type={!this.state.visible ? "password" : "text"} value={this.state.pwd}
                               onChange={(e, v) => this.change(v, "pwd")}/>
                    <FontIcon className={this.state.visible
                        ? "fa fa-eye pwd"
                        : "fa fa-eye-slash pwd"} onClick={() => this.pwdVisTog()}/><br/><br/>
                    <RaisedButton label="Войти" primary={true}
                                  onClick={() => this.props.login("77777777777", "Taurus85")}/>
                </div>
            </div>
        );
    }
}

export default Login;
