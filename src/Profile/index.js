import React, {Component} from 'react';

// Material-UI
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

// Font
import 'typeface-roboto'

class Profile extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      oldPwd: '',
      newPwd: '',
      newPwdOnceMore: '',
      phone: '',
      name: '',
      email: '',
      nameError: '',
      phoneError: '',
      emailError: '',
      oldPwdError: '',
      newPwdOnceMoreError: '',
      newPwdError: ''
    }
  }

  componentWillReceiveProps(props) {
    this.setState({'name': props.owner.name, 'phone': props.owner.phone, 'email': props.owner.email});
  }

  componentDidMount() {
    this.setState({'name': this.props.owner.name, 'phone': this.props.owner.phone, 'email': this.props.owner.email});
  }

  xhr = () => {
      let XHR = ("onload" in new XMLHttpRequest())
          ? XMLHttpRequest
          : XDomainRequest;
      let xhr = new XHR();
      xhr.withCredentials = true;
      return xhr
  }

  changeOwner = () => {
    let name=this.state.name;
    let phone=this.state.phone;
    let email=this.state.email;
    let newPwd=this.state.newPwd;
    let newPwdOnceMore=this.state.newPwdOnceMore;
    let oldPwd=this.state.oldPwd;

      let t=this;
      let xhr=this.xhr();
      xhr.open("POST", "https://betacab.gusli.net/api/v1/owner", true);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.onerror = function () {
          // debugger
          // alert('Ошибка ' + this.response);
      };
      xhr.onload = function () {
        let data=JSON.parse(this.response);
          if (data && !data.data) {
            if (data.error) {
              let obj={};
              let fails=data.error.fails;
              if (fails) {

                if (fails.email) {
                  obj.emailError=fails.email.join(' ')
                }

                if (fails.oldPassword) {
                  obj.oldPwdError=fails.oldPassword.join(' ')
                }

                if (fails.fio) {
                  obj.nameError=fails.fio.join(' ')
                }

                if (fails.phone) {
                  obj.phoneError=fails.phone.join(' ')
                }

                if (fails.password) {
                  obj.newPwdError=fails.password.join(' ')
                }

                if (fails.password_confirmation) {
                  obj.newPwdOnceMoreError=fails.password_confirmation.join(' ')
                }

                t.setState(obj);
              }
            }
          } else {
            data=data.data;
            t.props.renewOwner(data);
          }
      };
      let obj={'fio': name, 'email': email, 'phone': phone, 'oldPassword': oldPwd, 'password': newPwd ,'password_confirmation':newPwdOnceMore}
      //добавить поля, спрашивать старый пароль при изменении
      xhr.send(JSON.stringify(obj));
  }

  changePwd = (v,key) => {
    let o={}
    o[key]=v;
    this.setState(o);
  }

  changeText = (v,key) => {
    let obj={};
    obj[key]=v;
    this.setState(obj);
  }

  nameOut = () => {
    let name=this.state.name;
    if (name!==undefined && name.length>255) {
      this.setState({'nameError':'Слишком длинное значение'});
    }
    if (name!==undefined && name.length<1) {
      this.setState({'nameError':'Слишком короткое значение'});
    }
  }

  emailOut = () => {
    let email=this.state.email;
    if (email!==undefined && email.length>255) {
      this.setState({'emailError':'Слишком длинное значение'});
    }
    if (email!==undefined && email.length<7) {
      this.setState({'emailError':'Слишком короткое значение'});
    }
  }

  phoneOut = () => {
    let phone=this.state.phone;
    if (phone!==undefined && phone.length>50) {
      this.setState({'phoneError':'Слишком длинное значение'});
    }
    if (phone!==undefined && phone.length<9) {
      this.setState({'phoneError':'Слишком короткое значение'});
    }
  }

  newPwdOut = () => {
    let newPwd=this.state.newPwd;
    let newPwdOnceMore=this.state.newPwdOnceMore;
    let oldPwd=this.state.oldPwd;
    if (newPwd!==undefined && newPwd.length>0 && newPwd.length<6) {
      this.setState({'newPwdError':'Слишком короткое значение'});
    }
    if (newPwd==newPwdOnceMore && newPwdOnceMore!=='' && this.state.oldPwd==='') {
      this.setState({'oldPwdError':'Необходимо ввести текущий пароль'});
    }
  }

  newPwdOnceMoreOut = () => {
    let newPwd=this.state.newPwd;
    let newPwdOnceMore=this.state.newPwdOnceMore;
    let oldPwd=this.state.oldPwd;
    if (newPwd!==newPwdOnceMore) {
      this.setState({'newPwdOnceMoreError':'Введённые пароли не совпадают'});
    }
    if (newPwd===newPwdOnceMore && newPwdOnceMore!=='' && this.state.oldPwd==='') {
      this.setState({'oldPwdError':'Необходимо ввести текущий пароль'});
    }
  }

  oldPwdOut = () => {
    let newPwd=this.state.newPwd;
    let newPwdOnceMore=this.state.newPwdOnceMore;
    let oldPwd=this.state.oldPwd;
    if (newPwd!==undefined && newPwd.length<6) {
      this.setState({'newPwdError':'Слишком короткое значение'});
    }
    if (newPwd==newPwdOnceMore && newPwdOnceMore!=='' && this.state.oldPwd==='') {
      this.setState({'oldPwdError':'Необходимо ввести текущий пароль'});
    }
    if (newPwd==newPwdOnceMore && newPwdOnceMore!=='' && this.state.oldPwd.length<6) {
      this.setState({'oldPwdError':'Слишком короткое значение'});
    }
  }

  focus = (n) => {
    let obj={}
    obj[n]='';
    this.setState(obj);
  }

  render() {

    let disabled=true;

    let name=this.state.name;
    let phone=this.state.phone;
    let email=this.state.email;
    let newPwd=this.state.newPwd;
    let newPwdOnceMore=this.state.newPwdOnceMore;
    let oldPwd=this.state.oldPwd;

    if (!(name!==undefined && name.length>255) && !(name!==undefined && name.length<1) && !(phone!==undefined && phone.length>50) && !(phone!==undefined && phone.length<9) && !(email!==undefined && email.length>255) && !(email!==undefined && email.length<7) && !(newPwd.length>30) && !(newPwd.length>0 && newPwd.length<6) && !(newPwd!==newPwdOnceMore) && !(newPwd==newPwdOnceMore && newPwdOnceMore!=='' && (oldPwd==='' || oldPwd.length<6))) {
      disabled=false
    }

    if (this.state.nameError!=='' || this.state.phoneError!=='' || this.state.emailError!=='' || this.state.oldPwdError!=='' || this.state.newPwdOnceMoreError!=='' || this.state.newPwdError!=='') {
      disabled=true;
    }

    //debugger

    return (
      <div>
        <p className="pageHeader">Редактировать профиль</p>
        <div className="col">
          <p>Персональные данные</p>
          <TextField
            hintText="Имя"
            onBlur={() => this.nameOut()}
            onFocus={() => this.focus('nameError')}
            value={this.state.name}
            errorText={this.state.nameError}
            onChange={(e,v) => this.changeText(v,'name')}/>
          <br/>
          <TextField
            hintText="Телефон"
            onBlur={() => this.phoneOut()}
            onFocus={() => this.focus('phoneError')}
            value={this.state.phone}
            errorText={this.state.phoneError}
            onChange={(e,v) => this.changeText(v,'phone')}/>
          <br/>
          <TextField
            hintText="Email"
            onBlur={() => this.emailOut()}
            onFocus={() => this.focus('emailError')}
            errorText={this.state.emailError}
            value={this.state.email}
            onChange={(e,v) => this.changeText(v,'email')}/>
          <br/>
        </div>
        <div className="col">
          <p>Сменить пароль</p>
          <TextField
            hintText="Текущий пароль"
            onBlur={() => this.oldPwdOut()}
            onFocus={() => this.focus('oldPwdError')}
            errorText={this.state.oldPwdError}
            value={this.state.oldPwd} type="password"
            onChange={(e,v) => this.changePwd(v,'oldPwd')}/>
          <br/>
          <TextField
            hintText="Новый пароль"
            onBlur={() => this.newPwdOut()}
            onFocus={() => this.focus('newPwdError')}
            errorText={this.state.newPwdError}
            type="password" value={this.state.newPwd}
            onChange={(e,v) => this.changePwd(v,'newPwd')}/>
          <br/>
          <TextField
            hintText="Новый пароль ещё раз"
            onBlur={() => this.newPwdOnceMoreOut()}
            onFocus={() => this.focus('newPwdOnceMoreError')}
            errorText={this.state.newPwdOnceMoreError}
            type="password" value={this.state.newPwdOnceMore}
            onChange={(e,v) => this.changePwd(v,'newPwdOnceMore')}/>
          <br/>
        </div>
        <br/>
        <RaisedButton label="Сохранить" onClick={() => this.changeOwner()} disabled={disabled}/>
      </div>
    );
  }
}

export default Profile;
