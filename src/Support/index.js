import React, {Component} from 'react';

// Material-UI
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';

// Font
import 'typeface-roboto'

class Support extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      messages: [],
      text: "",
      more: true
    }
  }

  messageRequest (id) {
    let t=this;
    if (!id) {
      id=0
    }
    let XHR = ("onload" in new XMLHttpRequest())
        ? XMLHttpRequest
        : XDomainRequest;
    let xhr = new XHR();
    xhr.withCredentials = true;
    xhr.open("GET", "https://betacab.gusli.net/api/v1/chat/messages?last_id="+id, true);

    xhr.onload = function () {
        if (JSON.parse(this.responseText).status_code === 401) {
                window.location.replace('https://betacab.gusli.net/logout')
        } else {
            let data=JSON.parse(this.response).data.sort(function(a,b){return a.id>b.id?1:-1});
            let obj={messages: data}
            if (data.length<50) {
              obj.more=false;
            }
            t.setState(obj);
        }
    };
    xhr.onerror = function () {
        debugger
        //alert('Ошибка ' + this.status);
    };
    xhr.send();
  }

  timer=undefined

  pinger = () => {
    console.log('ping');
    let t=this;
    let messages=this.state.messages
    let lastId=messages[messages.length-1].id;

    let XHR = ("onload" in new XMLHttpRequest())
        ? XMLHttpRequest
        : XDomainRequest;
    let xhr = new XHR();
    xhr.withCredentials = true;
    xhr.open("GET", "https://betacab.gusli.net/api/v1/chat/messages?last_id="+lastId, true);

    xhr.onload = function () {
        if (JSON.parse(this.responseText).status_code === 401) {
                window.location.replace('https://betacab.gusli.net/logout')
        } else {
            if (this.response!=='[]') {
              messages=messages.concat(JSON.parse(this.response).data);
              messages.sort(function(a,b){return a.id>b.id?1:-1});
              t.setState({'messages':messages});
            }
        }
    };
    xhr.onerror = function () {
        debugger
        //alert('Ошибка ' + this.status);
    };
    xhr.send();
  }

  componentDidMount() {
    this.messageRequest();
    this.timer=setInterval(this.pinger,5000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  changeText = (v) => {
    this.setState({'text': v});
  }

  send = () => {
    let t=this;
    let text=this.state.text;
    if (text.length>0) {
      let XHR = ("onload" in new XMLHttpRequest())
          ? XMLHttpRequest
          : XDomainRequest;
      let xhr = new XHR();
      xhr.withCredentials = true;
      xhr.open("POST", "https://betacab.gusli.net/api/v1/chat/post", true);
      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      xhr.onload = function () {
          if (JSON.parse(this.responseText).status_code === 401) {
                  window.location.replace('https://betacab.gusli.net/logout')
          } else {
            let messages=t.state.messages;
            messages.push(JSON.parse(this.response).data);
            t.setState({'messages': messages, 'text': ''});
          }
      };
      xhr.onerror = function () {
          debugger
          //alert('Ошибка ' + this.status);
      };
      xhr.send("message=" + text);
    }
  }

  showMore = () => {
    let t=this;
    let id=this.state.messages[0].id;
    let XHR = ("onload" in new XMLHttpRequest())
        ? XMLHttpRequest
        : XDomainRequest;
    let xhr = new XHR();
    xhr.withCredentials = true;
    xhr.open("GET", "https://betacab.gusli.net/api/v1/chat/previousMessages?min_id="+id, true);

    xhr.onload = function () {
        if (JSON.parse(this.responseText).status_code === 401) {
                window.location.replace('https://betacab.gusli.net/logout')
        } else {
          let data=JSON.parse(this.response).data;
          if (data) {
            let messages=t.state.messages;
            messages=data.concat(messages).sort(function(a,b){return a.id>b.id?1:-1});
            t.setState({'messages':messages});
          } else {
            t.setState({'more': false});
          }

        }
    };
    xhr.onerror = function () {
        debugger
        //alert('Ошибка ' + this.status);
    };
    xhr.send();
  }

  render() {
    let messages = this.state.messages;
    return (
      <div>
        <div className="messages">
        <FlatButton className={!this.state.more?'':'hidden'} label="больше сообщений нет" disabled/>
        <FlatButton className={this.state.more?'':'hidden'} label="Показать предыдущие сообщения" onClick={() => this.showMore()}/>
          {messages.map((row, index) => (
            <div className={row.type} key={index}>
              <div className="mDate">{row.date}</div>
              <div className="mDate">{row.name}</div>
              <div className="mText">{row.message}</div>
            </div>
          ))}
        </div>
        <TextField hintText="Введите ваше сообщение" multiLine={true} value={this.state.text} onChange={(e, v) => this.changeText(v)}/><br/>
        <FlatButton label="Отправить" onClick={() => this.send()}/>
      </div>
    );
  }
}

export default Support;
